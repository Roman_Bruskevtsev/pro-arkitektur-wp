-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Бер 01 2023 р., 15:24
-- Версія сервера: 10.3.13-MariaDB-log
-- Версія PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `pro-arkitektur`
--

-- --------------------------------------------------------

--
-- Структура таблиці `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-12-23 10:20:18', '2021-12-23 10:20:18', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://pro-arkitektur-wp/build', 'yes'),
(2, 'home', 'http://pro-arkitektur-wp/build', 'yes'),
(3, 'blogname', 'PRO Arkitektur AS', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'example@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:21:\"polylang/polylang.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:33:\"duplicate-post/duplicate-post.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'pro-arkitektur', 'yes'),
(41, 'stylesheet', 'pro-arkitektur', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '0', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:0:{}', 'yes'),
(77, 'widget_text', 'a:0:{}', 'yes'),
(78, 'widget_rss', 'a:0:{}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '5', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1655806818', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'wp_force_deactivated_plugins', 'a:0:{}', 'yes'),
(99, 'initial_db_version', '49752', 'yes'),
(100, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(101, 'fresh_site', '0', 'yes'),
(102, 'widget_block', 'a:6:{i:2;a:1:{s:7:\"content\";s:19:\"<!-- wp:search /-->\";}i:3;a:1:{s:7:\"content\";s:154:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Recent Posts</h2><!-- /wp:heading --><!-- wp:latest-posts /--></div><!-- /wp:group -->\";}i:4;a:1:{s:7:\"content\";s:227:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Recent Comments</h2><!-- /wp:heading --><!-- wp:latest-comments {\"displayAvatar\":false,\"displayDate\":false,\"displayExcerpt\":false} /--></div><!-- /wp:group -->\";}i:5;a:1:{s:7:\"content\";s:146:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Archives</h2><!-- /wp:heading --><!-- wp:archives /--></div><!-- /wp:group -->\";}i:6;a:1:{s:7:\"content\";s:150:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Categories</h2><!-- /wp:heading --><!-- wp:categories /--></div><!-- /wp:group -->\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:3:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";}s:8:\"footer-1\";a:2:{i:0;s:7:\"block-5\";i:1;s:7:\"block-6\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:7:{i:1658326819;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1658355619;a:4:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1658398819;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1658398829;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1658398830;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1658485219;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(118, 'recovery_keys', 'a:0:{}', 'yes'),
(119, 'theme_mods_twentytwentyone', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1640255019;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";}s:9:\"sidebar-2\";a:2:{i:0;s:7:\"block-5\";i:1;s:7:\"block-6\";}}}}', 'yes'),
(120, 'https_detection_errors', 'a:1:{s:23:\"ssl_verification_failed\";a:1:{i:0;s:24:\"SSL verification failed.\";}}', 'yes'),
(121, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:6:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.0.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.0.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.0.1\";s:7:\"version\";s:5:\"6.0.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.9\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.0.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.0.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.0.1-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.0.1\";s:7:\"version\";s:5:\"6.0.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.9\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-6.0.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-6.0.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-6.0-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-6.0-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:3:\"6.0\";s:7:\"version\";s:3:\"6.0\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.9\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.9.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.9.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.9.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.9.3-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.9.3\";s:7:\"version\";s:5:\"5.9.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.9\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.9.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.9.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.9.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.9.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.9.2\";s:7:\"version\";s:5:\"5.9.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.9\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:5;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.8.4.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.8.4-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.8.4-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.8.4-partial-2.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.8.4-rollback-2.zip\";}s:7:\"current\";s:5:\"5.8.4\";s:7:\"version\";s:5:\"5.8.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.9\";s:15:\"partial_version\";s:5:\"5.8.2\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1658326611;s:15:\"version_checked\";s:5:\"5.8.2\";s:12:\"translations\";a:0:{}}', 'no'),
(123, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1658326612;s:8:\"response\";a:9:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.2.5\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.2.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.0\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";b:0;}s:27:\"autoptimize/autoptimize.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/autoptimize\";s:4:\"slug\";s:11:\"autoptimize\";s:6:\"plugin\";s:27:\"autoptimize/autoptimize.php\";s:11:\"new_version\";s:5:\"3.1.0\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/autoptimize/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/autoptimize.3.1.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/autoptimize/assets/icon-256X256.png?rev=2211608\";s:2:\"1x\";s:64:\"https://ps.w.org/autoptimize/assets/icon-128x128.png?rev=1864142\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/autoptimize/assets/banner-772x250.jpg?rev=1315920\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.9\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.6\";}s:23:\"loco-translate/loco.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:28:\"w.org/plugins/loco-translate\";s:4:\"slug\";s:14:\"loco-translate\";s:6:\"plugin\";s:23:\"loco-translate/loco.php\";s:11:\"new_version\";s:5:\"2.6.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/loco-translate/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/loco-translate.2.6.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-256x256.png?rev=1000676\";s:2:\"1x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-128x128.png?rev=1000676\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/loco-translate/assets/banner-772x250.jpg?rev=745046\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.2\";s:6:\"tested\";s:5:\"6.0.0\";s:12:\"requires_php\";s:6:\"5.6.20\";s:14:\"upgrade_notice\";s:54:\"<ul>\n<li>Various improvements and bug fixes</li>\n</ul>\";}s:21:\"polylang/polylang.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/polylang\";s:4:\"slug\";s:8:\"polylang\";s:6:\"plugin\";s:21:\"polylang/polylang.php\";s:11:\"new_version\";s:5:\"3.2.5\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/polylang/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/polylang.3.2.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:61:\"https://ps.w.org/polylang/assets/icon-256x256.png?rev=1331499\";s:2:\"1x\";s:61:\"https://ps.w.org/polylang/assets/icon-128x128.png?rev=1331499\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/polylang/assets/banner-1544x500.png?rev=1405299\";s:2:\"1x\";s:63:\"https://ps.w.org/polylang/assets/banner-772x250.png?rev=1405299\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.6\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.6\";}s:31:\"query-monitor/query-monitor.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/query-monitor\";s:4:\"slug\";s:13:\"query-monitor\";s:6:\"plugin\";s:31:\"query-monitor/query-monitor.php\";s:11:\"new_version\";s:5:\"3.9.0\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/query-monitor/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/query-monitor.3.9.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/query-monitor/assets/icon-256x256.png?rev=2301273\";s:2:\"1x\";s:58:\"https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073\";s:3:\"svg\";s:58:\"https://ps.w.org/query-monitor/assets/icon.svg?rev=2056073\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/query-monitor/assets/banner-1544x500.png?rev=2457098\";s:2:\"1x\";s:68:\"https://ps.w.org/query-monitor/assets/banner-772x250.png?rev=2457098\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"3.7\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:5:\"5.3.6\";}s:33:\"w3-total-cache/w3-total-cache.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/w3-total-cache\";s:4:\"slug\";s:14:\"w3-total-cache\";s:6:\"plugin\";s:33:\"w3-total-cache/w3-total-cache.php\";s:11:\"new_version\";s:5:\"2.2.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/w3-total-cache/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/w3-total-cache.2.2.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/w3-total-cache/assets/icon-256x256.png?rev=1041806\";s:2:\"1x\";s:67:\"https://ps.w.org/w3-total-cache/assets/icon-128x128.png?rev=1041806\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/w3-total-cache/assets/banner-772x250.jpg?rev=1041806\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"3.8\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:3:\"5.6\";}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:3:\"4.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/duplicate-post.4.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=2336666\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=2336666\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/duplicate-post/assets/banner-1544x500.png?rev=2336666\";s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=2336666\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.8\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:6:\"5.6.20\";}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"19.3\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.19.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2643727\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=2643727\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=2643727\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=2643727\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=2643727\";}s:8:\"requires\";s:3:\"5.8\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";s:6:\"5.6.20\";}s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.12.3\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"6.0.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:11:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:7:\"akismet\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"4.2.1\";s:7:\"updated\";s:19:\"2021-09-30 19:03:01\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/translation/plugin/akismet/4.2.1/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:7:\"akismet\";s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"4.2.1\";s:7:\"updated\";s:19:\"2020-03-23 16:37:49\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/translation/plugin/akismet/4.2.1/nn_NO.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"autoptimize\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"2.9.3\";s:7:\"updated\";s:19:\"2020-07-28 13:31:25\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/autoptimize/2.9.3/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:3;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"5.5.3\";s:7:\"updated\";s:19:\"2021-11-28 09:26:27\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.5.3/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:4;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:5:\"5.5.3\";s:7:\"updated\";s:19:\"2021-05-04 21:36:36\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.5.3/nn_NO.zip\";s:10:\"autoupdate\";b:1;}i:5;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"hello-dolly\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"1.7.2\";s:7:\"updated\";s:19:\"2018-03-20 19:24:06\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/hello-dolly/1.7.2/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:6;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"polylang\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"3.1.3\";s:7:\"updated\";s:19:\"2021-07-27 16:37:41\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/translation/plugin/polylang/3.1.3/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:7;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"query-monitor\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"3.7.1\";s:7:\"updated\";s:19:\"2021-05-13 14:38:18\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/translation/plugin/query-monitor/3.7.1/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:8;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"w3-total-cache\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"2.2.1\";s:7:\"updated\";s:19:\"2020-12-16 17:21:12\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/w3-total-cache/2.2.1/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:9;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"duplicate-post\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:3:\"4.3\";s:7:\"updated\";s:19:\"2021-08-15 10:08:43\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/duplicate-post/4.3/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:10;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:4:\"17.8\";s:7:\"updated\";s:19:\"2021-12-14 08:59:40\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/17.8/en_GB.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:3:{s:57:\"acf-options-for-polylang/bea-acf-options-for-polylang.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:38:\"w.org/plugins/acf-options-for-polylang\";s:4:\"slug\";s:24:\"acf-options-for-polylang\";s:6:\"plugin\";s:57:\"acf-options-for-polylang/bea-acf-options-for-polylang.php\";s:11:\"new_version\";s:6:\"1.1.10\";s:3:\"url\";s:55:\"https://wordpress.org/plugins/acf-options-for-polylang/\";s:7:\"package\";s:74:\"https://downloads.wordpress.org/plugin/acf-options-for-polylang.1.1.10.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/acf-options-for-polylang/assets/icon-256x256.png?rev=2504391\";s:2:\"1x\";s:77:\"https://ps.w.org/acf-options-for-polylang/assets/icon-128x128.png?rev=2504391\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/acf-options-for-polylang/assets/banner-1544x500.png?rev=2504391\";s:2:\"1x\";s:79:\"https://ps.w.org/acf-options-for-polylang/assets/banner-772x250.png?rev=2504391\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.7\";}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.6.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.6.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"5.9\";s:6:\"tested\";s:5:\"6.0.1\";s:12:\"requires_php\";b:0;}s:9:\"hello.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/hello-dolly/assets/banner-1544x500.jpg?rev=2645582\";s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.6\";}}}', 'no'),
(126, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1658326613;s:7:\"checked\";a:4:{s:14:\"pro-arkitektur\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"2.1\";s:12:\"twentytwenty\";s:3:\"1.8\";s:15:\"twentytwentyone\";s:3:\"1.4\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"2.3\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.2.3.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.2.0.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.6.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:14:\"twentynineteen\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:3:\"2.1\";s:7:\"updated\";s:19:\"2021-03-09 21:17:32\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/theme/twentynineteen/2.1/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:12:\"twentytwenty\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:3:\"1.8\";s:7:\"updated\";s:19:\"2020-12-10 16:45:41\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/theme/twentytwenty/1.8/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:15:\"twentytwentyone\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:3:\"1.0\";s:7:\"updated\";s:19:\"2022-01-26 08:46:08\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/theme/twentytwentyone/1.0/en_GB.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(132, 'can_compress_scripts', '1', 'no'),
(147, 'finished_updating_comment_type', '1', 'yes'),
(148, 'current_theme', 'PRO-Arkitektur', 'yes'),
(149, 'theme_mods_pro-arkitektur', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:4:\"main\";i:8;s:8:\"projects\";i:25;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(150, 'theme_switched', '', 'yes'),
(157, 'recently_activated', 'a:0:{}', 'yes'),
(158, 'acf_version', '5.11.4', 'yes'),
(159, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.5.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1640255260;s:7:\"version\";s:5:\"5.5.3\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(162, 'polylang', 'a:15:{s:7:\"browser\";i:0;s:7:\"rewrite\";i:1;s:12:\"hide_default\";i:1;s:10:\"force_lang\";i:1;s:13:\"redirect_lang\";i:0;s:13:\"media_support\";b:0;s:9:\"uninstall\";i:0;s:4:\"sync\";a:0:{}s:10:\"post_types\";a:1:{i:0;s:7:\"project\";}s:10:\"taxonomies\";a:1:{i:0;s:19:\"projects-categories\";}s:7:\"domains\";a:0:{}s:7:\"version\";s:5:\"3.1.3\";s:16:\"first_activation\";i:1640255263;s:12:\"default_lang\";s:2:\"en\";s:9:\"nav_menus\";a:1:{s:14:\"pro-arkitektur\";a:2:{s:4:\"main\";a:2:{s:2:\"en\";i:8;s:2:\"nn\";i:0;}s:8:\"projects\";a:2:{s:2:\"en\";i:25;s:2:\"nn\";i:0;}}}}', 'yes'),
(163, 'polylang_wpml_strings', 'a:0:{}', 'yes'),
(164, 'widget_polylang', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(175, 'pll_dismissed_notices', 'a:1:{i:0;s:6:\"wizard\";}', 'yes'),
(176, 'category_children', 'a:0:{}', 'yes'),
(177, 'options_logo', '34', 'no'),
(178, '_options_logo', 'field_61c4580cefe26', 'no'),
(179, 'options_logo_dark', '35', 'no'),
(180, '_options_logo_dark', 'field_61c4581eefe27', 'no'),
(181, 'options_copyrights', '© 2022 Pro Atkitektur AS', 'no'),
(182, '_options_copyrights', 'field_61c45865efe29', 'no'),
(192, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(233, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":6,\"critical\":2}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(248, 'rewrite_rules', 'a:225:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:53:\"^(nn)/wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:92:\"index.php?lang=$matches[1]&sitemap=$matches[2]&sitemap-subtype=$matches[3]&paged=$matches[4]\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:39:\"^(nn)/wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:64:\"index.php?lang=$matches[1]&sitemap=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:16:\"(nn)/projects/?$\";s:44:\"index.php?lang=$matches[1]&post_type=project\";s:11:\"projects/?$\";s:35:\"index.php?lang=en&post_type=project\";s:46:\"(nn)/projects/feed/(feed|rdf|rss|rss2|atom)/?$\";s:61:\"index.php?lang=$matches[1]&post_type=project&feed=$matches[2]\";s:41:\"projects/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?lang=en&post_type=project&feed=$matches[1]\";s:41:\"(nn)/projects/(feed|rdf|rss|rss2|atom)/?$\";s:61:\"index.php?lang=$matches[1]&post_type=project&feed=$matches[2]\";s:36:\"projects/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?lang=en&post_type=project&feed=$matches[1]\";s:33:\"(nn)/projects/page/([0-9]{1,})/?$\";s:62:\"index.php?lang=$matches[1]&post_type=project&paged=$matches[2]\";s:28:\"projects/page/([0-9]{1,})/?$\";s:53:\"index.php?lang=en&post_type=project&paged=$matches[1]\";s:52:\"(nn)/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?lang=$matches[1]&category_name=$matches[2]&feed=$matches[3]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:47:\"(nn)/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?lang=$matches[1]&category_name=$matches[2]&feed=$matches[3]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:28:\"(nn)/category/(.+?)/embed/?$\";s:63:\"index.php?lang=$matches[1]&category_name=$matches[2]&embed=true\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:40:\"(nn)/category/(.+?)/page/?([0-9]{1,})/?$\";s:70:\"index.php?lang=$matches[1]&category_name=$matches[2]&paged=$matches[3]\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:22:\"(nn)/category/(.+?)/?$\";s:52:\"index.php?lang=$matches[1]&category_name=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:49:\"(nn)/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:59:\"index.php?lang=$matches[1]&tag=$matches[2]&feed=$matches[3]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:44:\"(nn)/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:59:\"index.php?lang=$matches[1]&tag=$matches[2]&feed=$matches[3]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:25:\"(nn)/tag/([^/]+)/embed/?$\";s:53:\"index.php?lang=$matches[1]&tag=$matches[2]&embed=true\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:37:\"(nn)/tag/([^/]+)/page/?([0-9]{1,})/?$\";s:60:\"index.php?lang=$matches[1]&tag=$matches[2]&paged=$matches[3]\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:19:\"(nn)/tag/([^/]+)/?$\";s:42:\"index.php?lang=$matches[1]&tag=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:50:\"(nn)/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&post_format=$matches[2]&feed=$matches[3]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?lang=en&post_format=$matches[1]&feed=$matches[2]\";s:45:\"(nn)/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&post_format=$matches[2]&feed=$matches[3]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?lang=en&post_format=$matches[1]&feed=$matches[2]\";s:26:\"(nn)/type/([^/]+)/embed/?$\";s:61:\"index.php?lang=$matches[1]&post_format=$matches[2]&embed=true\";s:21:\"type/([^/]+)/embed/?$\";s:52:\"index.php?lang=en&post_format=$matches[1]&embed=true\";s:38:\"(nn)/type/([^/]+)/page/?([0-9]{1,})/?$\";s:68:\"index.php?lang=$matches[1]&post_format=$matches[2]&paged=$matches[3]\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:59:\"index.php?lang=en&post_format=$matches[1]&paged=$matches[2]\";s:20:\"(nn)/type/([^/]+)/?$\";s:50:\"index.php?lang=$matches[1]&post_format=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:41:\"index.php?lang=en&post_format=$matches[1]\";s:39:\"(nn)/projects/.+?/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:34:\"projects/.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"(nn)/projects/.+?/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:44:\"projects/.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"(nn)/projects/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:64:\"projects/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"(nn)/projects/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:59:\"projects/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"(nn)/projects/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:59:\"projects/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"(nn)/projects/.+?/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:40:\"projects/.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"(nn)/projects/(.+?)/embed/?$\";s:57:\"index.php?lang=$matches[1]&project=$matches[2]&embed=true\";s:23:\"projects/(.+?)/embed/?$\";s:40:\"index.php?project=$matches[1]&embed=true\";s:32:\"(nn)/projects/(.+?)/trackback/?$\";s:51:\"index.php?lang=$matches[1]&project=$matches[2]&tb=1\";s:27:\"projects/(.+?)/trackback/?$\";s:34:\"index.php?project=$matches[1]&tb=1\";s:52:\"(nn)/projects/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:63:\"index.php?lang=$matches[1]&project=$matches[2]&feed=$matches[3]\";s:47:\"projects/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?project=$matches[1]&feed=$matches[2]\";s:47:\"(nn)/projects/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:63:\"index.php?lang=$matches[1]&project=$matches[2]&feed=$matches[3]\";s:42:\"projects/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?project=$matches[1]&feed=$matches[2]\";s:40:\"(nn)/projects/(.+?)/page/?([0-9]{1,})/?$\";s:64:\"index.php?lang=$matches[1]&project=$matches[2]&paged=$matches[3]\";s:35:\"projects/(.+?)/page/?([0-9]{1,})/?$\";s:47:\"index.php?project=$matches[1]&paged=$matches[2]\";s:47:\"(nn)/projects/(.+?)/comment-page-([0-9]{1,})/?$\";s:64:\"index.php?lang=$matches[1]&project=$matches[2]&cpage=$matches[3]\";s:42:\"projects/(.+?)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?project=$matches[1]&cpage=$matches[2]\";s:36:\"(nn)/projects/(.+?)(?:/([0-9]+))?/?$\";s:63:\"index.php?lang=$matches[1]&project=$matches[2]&page=$matches[3]\";s:31:\"projects/(.+?)(?:/([0-9]+))?/?$\";s:46:\"index.php?project=$matches[1]&page=$matches[2]\";s:63:\"(nn)/projects-categories/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:75:\"index.php?lang=$matches[1]&projects-categories=$matches[2]&feed=$matches[3]\";s:58:\"projects-categories/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?projects-categories=$matches[1]&feed=$matches[2]\";s:58:\"(nn)/projects-categories/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:75:\"index.php?lang=$matches[1]&projects-categories=$matches[2]&feed=$matches[3]\";s:53:\"projects-categories/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?projects-categories=$matches[1]&feed=$matches[2]\";s:39:\"(nn)/projects-categories/(.+?)/embed/?$\";s:69:\"index.php?lang=$matches[1]&projects-categories=$matches[2]&embed=true\";s:34:\"projects-categories/(.+?)/embed/?$\";s:52:\"index.php?projects-categories=$matches[1]&embed=true\";s:51:\"(nn)/projects-categories/(.+?)/page/?([0-9]{1,})/?$\";s:76:\"index.php?lang=$matches[1]&projects-categories=$matches[2]&paged=$matches[3]\";s:46:\"projects-categories/(.+?)/page/?([0-9]{1,})/?$\";s:59:\"index.php?projects-categories=$matches[1]&paged=$matches[2]\";s:33:\"(nn)/projects-categories/(.+?)/?$\";s:58:\"index.php?lang=$matches[1]&projects-categories=$matches[2]\";s:28:\"projects-categories/(.+?)/?$\";s:41:\"index.php?projects-categories=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:37:\"(nn)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?lang=$matches[1]&&feed=$matches[2]\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:35:\"index.php?lang=en&&feed=$matches[1]\";s:32:\"(nn)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?lang=$matches[1]&&feed=$matches[2]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:35:\"index.php?lang=en&&feed=$matches[1]\";s:13:\"(nn)/embed/?$\";s:38:\"index.php?lang=$matches[1]&&embed=true\";s:8:\"embed/?$\";s:29:\"index.php?lang=en&&embed=true\";s:25:\"(nn)/page/?([0-9]{1,})/?$\";s:45:\"index.php?lang=$matches[1]&&paged=$matches[2]\";s:20:\"page/?([0-9]{1,})/?$\";s:36:\"index.php?lang=en&&paged=$matches[1]\";s:32:\"(nn)/comment-page-([0-9]{1,})/?$\";s:55:\"index.php?lang=$matches[1]&&page_id=5&cpage=$matches[2]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:46:\"index.php?lang=en&&page_id=5&cpage=$matches[1]\";s:7:\"(nn)/?$\";s:26:\"index.php?lang=$matches[1]\";s:46:\"(nn)/comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:59:\"index.php?lang=$matches[1]&&feed=$matches[2]&withcomments=1\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?lang=en&&feed=$matches[1]&withcomments=1\";s:41:\"(nn)/comments/(feed|rdf|rss|rss2|atom)/?$\";s:59:\"index.php?lang=$matches[1]&&feed=$matches[2]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?lang=en&&feed=$matches[1]&withcomments=1\";s:22:\"(nn)/comments/embed/?$\";s:38:\"index.php?lang=$matches[1]&&embed=true\";s:17:\"comments/embed/?$\";s:29:\"index.php?lang=en&&embed=true\";s:49:\"(nn)/search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?lang=$matches[1]&s=$matches[2]&feed=$matches[3]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?lang=en&s=$matches[1]&feed=$matches[2]\";s:44:\"(nn)/search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?lang=$matches[1]&s=$matches[2]&feed=$matches[3]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?lang=en&s=$matches[1]&feed=$matches[2]\";s:25:\"(nn)/search/(.+)/embed/?$\";s:51:\"index.php?lang=$matches[1]&s=$matches[2]&embed=true\";s:20:\"search/(.+)/embed/?$\";s:42:\"index.php?lang=en&s=$matches[1]&embed=true\";s:37:\"(nn)/search/(.+)/page/?([0-9]{1,})/?$\";s:58:\"index.php?lang=$matches[1]&s=$matches[2]&paged=$matches[3]\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?lang=en&s=$matches[1]&paged=$matches[2]\";s:19:\"(nn)/search/(.+)/?$\";s:40:\"index.php?lang=$matches[1]&s=$matches[2]\";s:14:\"search/(.+)/?$\";s:31:\"index.php?lang=en&s=$matches[1]\";s:52:\"(nn)/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&author_name=$matches[2]&feed=$matches[3]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?lang=en&author_name=$matches[1]&feed=$matches[2]\";s:47:\"(nn)/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&author_name=$matches[2]&feed=$matches[3]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?lang=en&author_name=$matches[1]&feed=$matches[2]\";s:28:\"(nn)/author/([^/]+)/embed/?$\";s:61:\"index.php?lang=$matches[1]&author_name=$matches[2]&embed=true\";s:23:\"author/([^/]+)/embed/?$\";s:52:\"index.php?lang=en&author_name=$matches[1]&embed=true\";s:40:\"(nn)/author/([^/]+)/page/?([0-9]{1,})/?$\";s:68:\"index.php?lang=$matches[1]&author_name=$matches[2]&paged=$matches[3]\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:59:\"index.php?lang=en&author_name=$matches[1]&paged=$matches[2]\";s:22:\"(nn)/author/([^/]+)/?$\";s:50:\"index.php?lang=$matches[1]&author_name=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:41:\"index.php?lang=en&author_name=$matches[1]\";s:74:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&feed=$matches[5]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:69:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&feed=$matches[5]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:50:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:91:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&embed=true\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:62:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:98:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&paged=$matches[5]\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:44:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:80:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:61:\"(nn)/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:81:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&feed=$matches[4]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:56:\"(nn)/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:81:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&feed=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:37:\"(nn)/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:75:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&embed=true\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:49:\"(nn)/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:82:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&paged=$matches[4]\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:31:\"(nn)/([0-9]{4})/([0-9]{1,2})/?$\";s:64:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:48:\"(nn)/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:60:\"index.php?lang=$matches[1]&year=$matches[2]&feed=$matches[3]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:43:\"(nn)/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:60:\"index.php?lang=$matches[1]&year=$matches[2]&feed=$matches[3]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:24:\"(nn)/([0-9]{4})/embed/?$\";s:54:\"index.php?lang=$matches[1]&year=$matches[2]&embed=true\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:36:\"(nn)/([0-9]{4})/page/?([0-9]{1,})/?$\";s:61:\"index.php?lang=$matches[1]&year=$matches[2]&paged=$matches[3]\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:18:\"(nn)/([0-9]{4})/?$\";s:43:\"index.php?lang=$matches[1]&year=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:63:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:73:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:93:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:88:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:88:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:69:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:58:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:108:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:62:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:102:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&tb=1\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:82:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:114:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&feed=$matches[6]\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:77:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:114:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&feed=$matches[6]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:70:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:115:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&paged=$matches[6]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:77:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:115:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&cpage=$matches[6]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:66:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:114:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&name=$matches[5]&page=$matches[6]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:52:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:62:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:82:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:77:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:77:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:58:\"(nn)/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:69:\"(nn)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&cpage=$matches[5]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:56:\"(nn)/([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:82:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:43:\"(nn)/([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:61:\"index.php?lang=$matches[1]&year=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:32:\"(nn)/.?.+?/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"(nn)/.?.+?/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"(nn)/.?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"(nn)/.?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"(nn)/.?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"(nn)/.?.+?/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"(nn)/(.?.+?)/embed/?$\";s:58:\"index.php?lang=$matches[1]&pagename=$matches[2]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:25:\"(nn)/(.?.+?)/trackback/?$\";s:52:\"index.php?lang=$matches[1]&pagename=$matches[2]&tb=1\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:45:\"(nn)/(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?lang=$matches[1]&pagename=$matches[2]&feed=$matches[3]\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:40:\"(nn)/(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?lang=$matches[1]&pagename=$matches[2]&feed=$matches[3]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:33:\"(nn)/(.?.+?)/page/?([0-9]{1,})/?$\";s:65:\"index.php?lang=$matches[1]&pagename=$matches[2]&paged=$matches[3]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:40:\"(nn)/(.?.+?)/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?lang=$matches[1]&pagename=$matches[2]&cpage=$matches[3]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:29:\"(nn)/(.?.+?)(?:/([0-9]+))?/?$\";s:64:\"index.php?lang=$matches[1]&pagename=$matches[2]&page=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(286, 'projects-categories_children', 'a:0:{}', 'yes'),
(302, 'duplicate_post_copytitle', '1', 'yes'),
(303, 'duplicate_post_copydate', '', 'yes'),
(304, 'duplicate_post_copystatus', '', 'yes'),
(305, 'duplicate_post_copyslug', '', 'yes'),
(306, 'duplicate_post_copyexcerpt', '1', 'yes'),
(307, 'duplicate_post_copycontent', '1', 'yes'),
(308, 'duplicate_post_copythumbnail', '1', 'yes'),
(309, 'duplicate_post_copytemplate', '1', 'yes'),
(310, 'duplicate_post_copyformat', '1', 'yes'),
(311, 'duplicate_post_copyauthor', '', 'yes'),
(312, 'duplicate_post_copypassword', '', 'yes'),
(313, 'duplicate_post_copyattachments', '', 'yes'),
(314, 'duplicate_post_copychildren', '', 'yes'),
(315, 'duplicate_post_copycomments', '', 'yes'),
(316, 'duplicate_post_copymenuorder', '1', 'yes'),
(317, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(318, 'duplicate_post_blacklist', '', 'yes'),
(319, 'duplicate_post_types_enabled', 'a:3:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:7:\"project\";}', 'yes'),
(320, 'duplicate_post_show_original_column', '', 'yes'),
(321, 'duplicate_post_show_original_in_post_states', '', 'yes'),
(322, 'duplicate_post_show_original_meta_box', '', 'yes'),
(323, 'duplicate_post_show_link', 'a:3:{s:9:\"new_draft\";s:1:\"1\";s:5:\"clone\";s:1:\"1\";s:17:\"rewrite_republish\";s:1:\"1\";}', 'yes'),
(324, 'duplicate_post_show_link_in', 'a:4:{s:3:\"row\";s:1:\"1\";s:8:\"adminbar\";s:1:\"1\";s:9:\"submitbox\";s:1:\"1\";s:11:\"bulkactions\";s:1:\"1\";}', 'yes'),
(325, 'duplicate_post_show_notice', '1', 'no'),
(326, 'duplicate_post_version', '4.3', 'yes'),
(327, 'duplicate_post_title_prefix', '', 'yes'),
(328, 'duplicate_post_title_suffix', '', 'yes'),
(329, 'duplicate_post_increase_menu_order_by', '', 'yes'),
(330, 'duplicate_post_roles', 'a:2:{i:0;s:13:\"administrator\";i:1;s:6:\"editor\";}', 'yes'),
(473, 'options_address_label', 'Visit our office:', 'no'),
(474, '_options_address_label', 'field_61cb66b2c8fba', 'no'),
(475, 'options_address_text', 'Strandgata 22, 9008 \r\nTromsø, Norway', 'no'),
(476, '_options_address_text', 'field_61cb66bfc8fbb', 'no'),
(477, 'options_address', '', 'no'),
(478, '_options_address', 'field_61cb660fc8fb4', 'no'),
(479, 'options_phone_numbers_label', 'Call us:', 'no'),
(480, '_options_phone_numbers_label', 'field_61cb666dc8fb7', 'no'),
(481, 'options_phone_numbers_phones_0_phone', '+47 958 89 442', 'no'),
(482, '_options_phone_numbers_phones_0_phone', 'field_61cb6677c8fb9', 'no'),
(483, 'options_phone_numbers_phones_1_phone', '+47 403 10 649', 'no'),
(484, '_options_phone_numbers_phones_1_phone', 'field_61cb6677c8fb9', 'no'),
(485, 'options_phone_numbers_phones', '2', 'no'),
(486, '_options_phone_numbers_phones', 'field_61cb6672c8fb8', 'no'),
(487, 'options_phone_numbers', '', 'no'),
(488, '_options_phone_numbers', 'field_61cb6627c8fb5', 'no'),
(489, 'options_email_label', 'Write us:', 'no'),
(490, '_options_email_label', 'field_61cb66f2c8fbd', 'no'),
(491, 'options_email_email', 'post@proarkitektur.no', 'no'),
(492, '_options_email_email', 'field_61cb66f7c8fbe', 'no'),
(493, 'options_email', '', 'no'),
(494, '_options_email', 'field_61cb66e4c8fbc', 'no'),
(495, 'options_social_networks_label', 'Follow us:', 'no'),
(496, '_options_social_networks_label', 'field_61cb6717c8fc0', 'no'),
(497, 'options_social_networks_facebook', 'https://www.facebook.com/', 'no'),
(498, '_options_social_networks_facebook', 'field_61cb671fc8fc1', 'no'),
(499, 'options_social_networks_instagram', 'https://www.instagram.com/', 'no'),
(500, '_options_social_networks_instagram', 'field_61cb672ec8fc2', 'no'),
(501, 'options_social_networks', '', 'no'),
(502, '_options_social_networks', 'field_61cb670ac8fbf', 'no'),
(606, '_transient_timeout_acf_plugin_updates', '1658499412', 'no'),
(607, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:6:\"5.12.3\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"6.0.1\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.11.4\";}}', 'no'),
(608, '_site_transient_timeout_theme_roots', '1658328413', 'no'),
(609, '_site_transient_theme_roots', 'a:4:{s:14:\"pro-arkitektur\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}', 'no'),
(610, '_site_transient_timeout_php_check_a5b4d2808570efd012607394df5c6fa9', '1658931414', 'no'),
(611, '_site_transient_php_check_a5b4d2808570efd012607394df5c6fa9', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(641, '_transient_pll_languages_list', 'a:2:{i:0;a:24:{s:7:\"term_id\";i:2;s:4:\"name\";s:3:\"Eng\";s:4:\"slug\";s:2:\"en\";s:10:\"term_group\";i:0;s:16:\"term_taxonomy_id\";i:2;s:5:\"count\";i:14;s:10:\"tl_term_id\";i:3;s:19:\"tl_term_taxonomy_id\";i:3;s:8:\"tl_count\";i:9;s:6:\"locale\";s:5:\"en_GB\";s:6:\"is_rtl\";i:0;s:3:\"w3c\";s:5:\"en-GB\";s:8:\"facebook\";s:5:\"en_GB\";s:8:\"home_url\";s:31:\"http://pro-arkitektur-wp/build/\";s:10:\"search_url\";s:31:\"http://pro-arkitektur-wp/build/\";s:4:\"host\";N;s:5:\"mo_id\";s:2:\"13\";s:13:\"page_on_front\";i:5;s:14:\"page_for_posts\";b:0;s:9:\"flag_code\";s:2:\"gb\";s:8:\"flag_url\";s:71:\"http://pro-arkitektur-wp/build/wp-content/plugins/polylang/flags/gb.png\";s:4:\"flag\";s:632:\"<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAt1BMVEWSmb66z+18msdig8La3u+tYX9IaLc7W7BagbmcUW+kqMr/q6n+//+hsNv/lIr/jIGMnNLJyOP9/fyQttT/wb3/////aWn+YWF5kNT0oqz0i4ueqtIZNJjhvt/8gn//WVr/6+rN1+o9RKZwgcMPJpX/VFT9UEn+RUX8Ozv2Ly+FGzdYZrfU1e/8LS/lQkG/mbVUX60AE231hHtcdMb0mp3qYFTFwNu3w9prcqSURGNDaaIUMX5FNW5wYt7AAAAAjklEQVR4AR3HNUJEMQCGwf+L8RR36ajR+1+CEuvRdd8kK9MNAiRQNgJmVDAt1yM6kSzYVJUsPNssAk5N7ZFKjVNFAY4co6TAOI+kyQm+LFUEBEKKzuWUNB7rSH/rSnvOulOGk+QlXTBqMIrfYX4tSe2nP3iRa/KNK7uTmWJ5a9+erZ3d+18od4ytiZdvZyuKWy8o3UpTVAAAAABJRU5ErkJggg==\" alt=\"Eng\" width=\"16\" height=\"11\" style=\"width: 16px; height: 11px;\" />\";s:15:\"custom_flag_url\";N;s:11:\"custom_flag\";N;}i:1;a:24:{s:7:\"term_id\";i:4;s:4:\"name\";s:3:\"Nor\";s:4:\"slug\";s:2:\"nn\";s:10:\"term_group\";i:0;s:16:\"term_taxonomy_id\";i:4;s:5:\"count\";i:1;s:10:\"tl_term_id\";i:5;s:19:\"tl_term_taxonomy_id\";i:5;s:8:\"tl_count\";i:0;s:6:\"locale\";s:5:\"nn_NO\";s:6:\"is_rtl\";i:0;s:3:\"w3c\";s:5:\"nn-NO\";s:8:\"facebook\";s:5:\"nn_NO\";s:8:\"home_url\";s:58:\"http://pro-arkitektur-wp/build/nn/home-page-norsk-nynorsk/\";s:10:\"search_url\";s:34:\"http://pro-arkitektur-wp/build/nn/\";s:4:\"host\";N;s:5:\"mo_id\";s:2:\"14\";s:13:\"page_on_front\";i:15;s:14:\"page_for_posts\";b:0;s:9:\"flag_code\";s:2:\"no\";s:8:\"flag_url\";s:71:\"http://pro-arkitektur-wp/build/wp-content/plugins/polylang/flags/no.png\";s:4:\"flag\";s:424:\"<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAS1BMVEXiAAD+/v4EVNHaAQHPAADyeniUuevvamjuXlzqS0jBAAB6puXoPz3lMjDjJyX6+vr19fXm5uZuneJild5QiNkADK/fExG0AADu7u75QkE3AAAAX0lEQVR4AQXBWQoCMRAFwHrpjhsI3v+OfgtmnFgVJHsXBDEdc/UgRJo65mpKZBkAJaIkeDQE0unpSgj26L3u/Ahw9ujvhTPbDZ/HoES44R2p6Xr0Vwlxb4fXr5bTE/wBnWoXiyWjuXcAAAAASUVORK5CYII=\" alt=\"Nor\" width=\"16\" height=\"11\" style=\"width: 16px; height: 11px;\" />\";s:15:\"custom_flag_url\";N;s:11:\"custom_flag\";N;}}', 'yes');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_lock', '1640625210:1'),
(4, 7, '_edit_lock', '1640720903:1'),
(5, 9, '_edit_lock', '1641214474:1'),
(9, 12, '_form', '<label> Your name\n    [text* your-name] </label>\n\n<label> Your email\n    [email* your-email] </label>\n\n<label> Subject\n    [text* your-subject] </label>\n\n<label> Your message (optional)\n    [textarea your-message] </label>\n\n[submit \"Submit\"]'),
(10, 12, '_mail', 'a:8:{s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:43:\"[_site_title] <wordpress@pro-arkitektur-wp>\";s:4:\"body\";s:163:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(11, 12, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:43:\"[_site_title] <wordpress@pro-arkitektur-wp>\";s:4:\"body\";s:105:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(12, 12, '_messages', 'a:12:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";}'),
(13, 12, '_additional_settings', ''),
(14, 12, '_locale', 'en_US'),
(15, 13, '_pll_strings_translations', 'a:0:{}'),
(16, 14, '_pll_strings_translations', 'a:0:{}'),
(17, 16, '_edit_last', '1'),
(18, 16, '_edit_lock', '1640720415:1'),
(19, 19, '_edit_last', '1'),
(20, 19, '_edit_lock', '1640628114:1'),
(21, 28, '_edit_last', '1'),
(22, 28, '_edit_lock', '1640720257:1'),
(23, 34, '_wp_attached_file', '2021/12/logo.svg'),
(24, 35, '_wp_attached_file', '2021/12/logo-dark.svg'),
(25, 15, '_edit_lock', '1640258137:1'),
(26, 15, '_edit_last', '1'),
(27, 15, 'content', ''),
(28, 15, '_content', 'field_61c44fc0720a9'),
(29, 36, 'content', ''),
(30, 36, '_content', 'field_61c44fc0720a9'),
(31, 37, '_edit_last', '1'),
(32, 37, 'gallery', 'a:5:{i:0;s:2:\"71\";i:1;s:2:\"73\";i:2;s:2:\"75\";i:3;s:2:\"77\";i:4;s:2:\"79\";}'),
(33, 37, '_gallery', 'field_61c450eee6ced'),
(34, 37, 'type', 'Single family house'),
(35, 37, '_type', 'field_61c4512fe6cee'),
(36, 37, 'location', 'Tromsø, Norway'),
(37, 37, '_location', 'field_61c45136e6cef'),
(38, 37, 'year', '2019'),
(39, 37, '_year', 'field_61c4513ee6cf0'),
(40, 37, 'description', 'Private villa located on top of southern Tromsø. Minimalistic and sharp design with obsession to every detail makes this project very unique. It has majestatic view against the south, the panorama view from this house is difficult to describe. It\'s better to see it with your own eyes here <a href=\"http://pro-arkitektur-wp/build/\">@villatromso</a>. It was pure pleasure to design this house.'),
(41, 37, '_description', 'field_61c45150e6cf1'),
(42, 37, '_edit_lock', '1641211555:1'),
(43, 38, '_menu_item_type', 'post_type_archive'),
(44, 38, '_menu_item_menu_item_parent', '0'),
(45, 38, '_menu_item_object_id', '-12'),
(46, 38, '_menu_item_object', 'project'),
(47, 38, '_menu_item_target', ''),
(48, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(49, 38, '_menu_item_xfn', ''),
(50, 38, '_menu_item_url', ''),
(52, 39, '_menu_item_type', 'post_type'),
(53, 39, '_menu_item_menu_item_parent', '0'),
(54, 39, '_menu_item_object_id', '9'),
(55, 39, '_menu_item_object', 'page'),
(56, 39, '_menu_item_target', ''),
(57, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(58, 39, '_menu_item_xfn', ''),
(59, 39, '_menu_item_url', ''),
(61, 40, '_menu_item_type', 'post_type'),
(62, 40, '_menu_item_menu_item_parent', '0'),
(63, 40, '_menu_item_object_id', '7'),
(64, 40, '_menu_item_object', 'page'),
(65, 40, '_menu_item_target', ''),
(66, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(67, 40, '_menu_item_xfn', ''),
(68, 40, '_menu_item_url', ''),
(70, 59, '_wp_attached_file', '2021/12/marker.svg'),
(71, 7, '_edit_last', '1'),
(72, 7, 'content_0_map_latitude', '69.6473059'),
(73, 7, '_content_0_map_latitude', 'field_61c4a2ed248a0'),
(74, 7, 'content_0_map_longitude', '18.9540747'),
(75, 7, '_content_0_map_longitude', 'field_61c4a301248a1'),
(76, 7, 'content_0_map_marker', '59'),
(77, 7, '_content_0_map_marker', 'field_61c4a30e248a2'),
(78, 7, 'content_0_map_zoom', '17'),
(79, 7, '_content_0_map_zoom', 'field_61c4a326248a3'),
(80, 7, 'content_0_map', ''),
(81, 7, '_content_0_map', 'field_61c4a2e02489f'),
(82, 7, 'content_0_address_label', 'Visit our office:'),
(83, 7, '_content_0_address_label', 'field_61c4a3e9248a5'),
(84, 7, 'content_0_address_text', 'Strandgata 22, 9008 \r\nTromsø, Norway'),
(85, 7, '_content_0_address_text', 'field_61c4a3ef248a6'),
(86, 7, 'content_0_address', ''),
(87, 7, '_content_0_address', 'field_61c4a3de248a4'),
(88, 7, 'content_0_phone_numbers_label', 'Call us:'),
(89, 7, '_content_0_phone_numbers_label', 'field_61c4a41a248a8'),
(90, 7, 'content_0_phone_numbers', ''),
(91, 7, '_content_0_phone_numbers', 'field_61c4a408248a7'),
(92, 7, 'content_0_email_label', 'Write us:'),
(93, 7, '_content_0_email_label', 'field_61c4a44d248ab'),
(94, 7, 'content_0_email_email', 'post@proarkitektur.no'),
(95, 7, '_content_0_email_email', 'field_61c4a458248ac'),
(96, 7, 'content_0_email', ''),
(97, 7, '_content_0_email', 'field_61c4a43d248aa'),
(98, 7, 'content_0_social_networks_label', 'Follow us:'),
(99, 7, '_content_0_social_networks_label', 'field_61c4a473248ae'),
(100, 7, 'content_0_social_networks_facebook', 'https://www.facebook.com/'),
(101, 7, '_content_0_social_networks_facebook', 'field_61c4a47c248af'),
(102, 7, 'content_0_social_networks_instagram', 'https://www.instagram.com/'),
(103, 7, '_content_0_social_networks_instagram', 'field_61c4a48b248b0'),
(104, 7, 'content_0_social_networks', ''),
(105, 7, '_content_0_social_networks', 'field_61c4a463248ad'),
(106, 7, 'content', 'a:1:{i:0;s:26:\"map_contacts_block_section\";}'),
(107, 7, '_content', 'field_61c44fc0720a9'),
(108, 60, 'content_0_map_latitude', '69.6473059'),
(109, 60, '_content_0_map_latitude', 'field_61c4a2ed248a0'),
(110, 60, 'content_0_map_longitude', '18.9540747'),
(111, 60, '_content_0_map_longitude', 'field_61c4a301248a1'),
(112, 60, 'content_0_map_marker', '59'),
(113, 60, '_content_0_map_marker', 'field_61c4a30e248a2'),
(114, 60, 'content_0_map_zoom', '17'),
(115, 60, '_content_0_map_zoom', 'field_61c4a326248a3'),
(116, 60, 'content_0_map', ''),
(117, 60, '_content_0_map', 'field_61c4a2e02489f'),
(118, 60, 'content_0_address_label', 'Visit our office:'),
(119, 60, '_content_0_address_label', 'field_61c4a3e9248a5'),
(120, 60, 'content_0_address_text', 'Strandgata 22, 9008 \r\nTromsø, Norway'),
(121, 60, '_content_0_address_text', 'field_61c4a3ef248a6'),
(122, 60, 'content_0_address', ''),
(123, 60, '_content_0_address', 'field_61c4a3de248a4'),
(124, 60, 'content_0_phone_numbers_label', 'Call us:'),
(125, 60, '_content_0_phone_numbers_label', 'field_61c4a41a248a8'),
(126, 60, 'content_0_phone_numbers', ''),
(127, 60, '_content_0_phone_numbers', 'field_61c4a408248a7'),
(128, 60, 'content_0_email_label', 'Write us:'),
(129, 60, '_content_0_email_label', 'field_61c4a44d248ab'),
(130, 60, 'content_0_email_email', 'post@proarkitektur.no'),
(131, 60, '_content_0_email_email', 'field_61c4a458248ac'),
(132, 60, 'content_0_email', ''),
(133, 60, '_content_0_email', 'field_61c4a43d248aa'),
(134, 60, 'content_0_social_networks_label', 'Follow us:'),
(135, 60, '_content_0_social_networks_label', 'field_61c4a473248ae'),
(136, 60, 'content_0_social_networks_facebook', 'https://www.facebook.com/'),
(137, 60, '_content_0_social_networks_facebook', 'field_61c4a47c248af'),
(138, 60, 'content_0_social_networks_instagram', 'https://www.instagram.com/'),
(139, 60, '_content_0_social_networks_instagram', 'field_61c4a48b248b0'),
(140, 60, 'content_0_social_networks', ''),
(141, 60, '_content_0_social_networks', 'field_61c4a463248ad'),
(142, 60, 'content', 'a:1:{i:0;s:26:\"map_contacts_block_section\";}'),
(143, 60, '_content', 'field_61c44fc0720a9'),
(144, 7, 'content_0_phone_numbers_phones_0_phone', '+47 958 89 442'),
(145, 7, '_content_0_phone_numbers_phones_0_phone', 'field_61c4a748efbcd'),
(146, 7, 'content_0_phone_numbers_phones_1_phone', '+47 403 10 649'),
(147, 7, '_content_0_phone_numbers_phones_1_phone', 'field_61c4a748efbcd'),
(148, 7, 'content_0_phone_numbers_phones', '2'),
(149, 7, '_content_0_phone_numbers_phones', 'field_61c4a424248a9'),
(150, 62, 'content_0_map_latitude', '69.6473059'),
(151, 62, '_content_0_map_latitude', 'field_61c4a2ed248a0'),
(152, 62, 'content_0_map_longitude', '18.9540747'),
(153, 62, '_content_0_map_longitude', 'field_61c4a301248a1'),
(154, 62, 'content_0_map_marker', '59'),
(155, 62, '_content_0_map_marker', 'field_61c4a30e248a2'),
(156, 62, 'content_0_map_zoom', '17'),
(157, 62, '_content_0_map_zoom', 'field_61c4a326248a3'),
(158, 62, 'content_0_map', ''),
(159, 62, '_content_0_map', 'field_61c4a2e02489f'),
(160, 62, 'content_0_address_label', 'Visit our office:'),
(161, 62, '_content_0_address_label', 'field_61c4a3e9248a5'),
(162, 62, 'content_0_address_text', 'Strandgata 22, 9008 \r\nTromsø, Norway'),
(163, 62, '_content_0_address_text', 'field_61c4a3ef248a6'),
(164, 62, 'content_0_address', ''),
(165, 62, '_content_0_address', 'field_61c4a3de248a4'),
(166, 62, 'content_0_phone_numbers_label', 'Call us:'),
(167, 62, '_content_0_phone_numbers_label', 'field_61c4a41a248a8'),
(168, 62, 'content_0_phone_numbers', ''),
(169, 62, '_content_0_phone_numbers', 'field_61c4a408248a7'),
(170, 62, 'content_0_email_label', 'Write us:'),
(171, 62, '_content_0_email_label', 'field_61c4a44d248ab'),
(172, 62, 'content_0_email_email', 'post@proarkitektur.no'),
(173, 62, '_content_0_email_email', 'field_61c4a458248ac'),
(174, 62, 'content_0_email', ''),
(175, 62, '_content_0_email', 'field_61c4a43d248aa'),
(176, 62, 'content_0_social_networks_label', 'Follow us:'),
(177, 62, '_content_0_social_networks_label', 'field_61c4a473248ae'),
(178, 62, 'content_0_social_networks_facebook', 'https://www.facebook.com/'),
(179, 62, '_content_0_social_networks_facebook', 'field_61c4a47c248af'),
(180, 62, 'content_0_social_networks_instagram', 'https://www.instagram.com/'),
(181, 62, '_content_0_social_networks_instagram', 'field_61c4a48b248b0'),
(182, 62, 'content_0_social_networks', ''),
(183, 62, '_content_0_social_networks', 'field_61c4a463248ad'),
(184, 62, 'content', 'a:1:{i:0;s:26:\"map_contacts_block_section\";}'),
(185, 62, '_content', 'field_61c44fc0720a9'),
(186, 62, 'content_0_phone_numbers_phones_0_phone', '+47 958 89 442'),
(187, 62, '_content_0_phone_numbers_phones_0_phone', 'field_61c4a748efbcd'),
(188, 62, 'content_0_phone_numbers_phones_1_phone', '+47 403 10 649'),
(189, 62, '_content_0_phone_numbers_phones_1_phone', 'field_61c4a748efbcd'),
(190, 62, 'content_0_phone_numbers_phones', '2'),
(191, 62, '_content_0_phone_numbers_phones', 'field_61c4a424248a9'),
(192, 63, 'content_0_map_latitude', '69.6473059'),
(193, 63, '_content_0_map_latitude', 'field_61c4a2ed248a0'),
(194, 63, 'content_0_map_longitude', '18.9540747'),
(195, 63, '_content_0_map_longitude', 'field_61c4a301248a1'),
(196, 63, 'content_0_map_marker', '59'),
(197, 63, '_content_0_map_marker', 'field_61c4a30e248a2'),
(198, 63, 'content_0_map_zoom', '17'),
(199, 63, '_content_0_map_zoom', 'field_61c4a326248a3'),
(200, 63, 'content_0_map', ''),
(201, 63, '_content_0_map', 'field_61c4a2e02489f'),
(202, 63, 'content_0_address_label', 'Visit our office:'),
(203, 63, '_content_0_address_label', 'field_61c4a3e9248a5'),
(204, 63, 'content_0_address_text', 'Strandgata 22, 9008 \r\nTromsø, Norway'),
(205, 63, '_content_0_address_text', 'field_61c4a3ef248a6'),
(206, 63, 'content_0_address', ''),
(207, 63, '_content_0_address', 'field_61c4a3de248a4'),
(208, 63, 'content_0_phone_numbers_label', 'Call us:'),
(209, 63, '_content_0_phone_numbers_label', 'field_61c4a41a248a8'),
(210, 63, 'content_0_phone_numbers', ''),
(211, 63, '_content_0_phone_numbers', 'field_61c4a408248a7'),
(212, 63, 'content_0_email_label', 'Write us:'),
(213, 63, '_content_0_email_label', 'field_61c4a44d248ab'),
(214, 63, 'content_0_email_email', 'post@proarkitektur.no'),
(215, 63, '_content_0_email_email', 'field_61c4a458248ac'),
(216, 63, 'content_0_email', ''),
(217, 63, '_content_0_email', 'field_61c4a43d248aa'),
(218, 63, 'content_0_social_networks_label', 'Follow us:'),
(219, 63, '_content_0_social_networks_label', 'field_61c4a473248ae'),
(220, 63, 'content_0_social_networks_facebook', 'https://www.facebook.com/'),
(221, 63, '_content_0_social_networks_facebook', 'field_61c4a47c248af'),
(222, 63, 'content_0_social_networks_instagram', 'https://www.instagram.com/'),
(223, 63, '_content_0_social_networks_instagram', 'field_61c4a48b248b0'),
(224, 63, 'content_0_social_networks', ''),
(225, 63, '_content_0_social_networks', 'field_61c4a463248ad'),
(226, 63, 'content', 'a:1:{i:0;s:26:\"map_contacts_block_section\";}'),
(227, 63, '_content', 'field_61c44fc0720a9'),
(228, 63, 'content_0_phone_numbers_phones_0_phone', '+47 958 89 442'),
(229, 63, '_content_0_phone_numbers_phones_0_phone', 'field_61c4a748efbcd'),
(230, 63, 'content_0_phone_numbers_phones_1_phone', '+47 403 10 649'),
(231, 63, '_content_0_phone_numbers_phones_1_phone', 'field_61c4a748efbcd'),
(232, 63, 'content_0_phone_numbers_phones', '2'),
(233, 63, '_content_0_phone_numbers_phones', 'field_61c4a424248a9'),
(234, 9, '_edit_last', '1'),
(235, 9, 'content_0_service_blocks_0_title', 'Architecture'),
(236, 9, '_content_0_service_blocks_0_title', 'field_61c4a9264c0cd'),
(237, 9, 'content_0_service_blocks_0_text', '<ul>\r\n 	<li>projects of buildings and objects of all categories</li>\r\n 	<li>concepts, sketches, visualizations, renderings, 3d modelling</li>\r\n 	<li>architecture design</li>\r\n 	<li>executive projects</li>\r\n 	<li>details design/detail drawings</li>\r\n 	<li>interior design</li>\r\n</ul>'),
(238, 9, '_content_0_service_blocks_0_text', 'field_61c4a92f4c0ce'),
(239, 9, 'content_0_service_blocks_1_title', 'Engineering'),
(240, 9, '_content_0_service_blocks_1_title', 'field_61c4a9264c0cd'),
(241, 9, 'content_0_service_blocks_1_text', '<ul>\r\n 	<li>building construction systems design</li>\r\n 	<li>building information modeling (BIM)</li>\r\n 	<li>structural design</li>\r\n 	<li>static calculations</li>\r\n</ul>'),
(242, 9, '_content_0_service_blocks_1_text', 'field_61c4a92f4c0ce'),
(243, 9, 'content_0_service_blocks_2_title', 'Management'),
(244, 9, '_content_0_service_blocks_2_title', 'field_61c4a9264c0cd'),
(245, 9, 'content_0_service_blocks_2_text', '<ul>\r\n 	<li>preparation of technical documentation at all stages of design in cooperation with all involved design teams</li>\r\n 	<li>all administration, management coordination and supervision of the project</li>\r\n 	<li>coordination and support for construction process</li>\r\n 	<li>cooperation with internal and external specialists</li>\r\n 	<li>achieving building permissions</li>\r\n</ul>'),
(246, 9, '_content_0_service_blocks_2_text', 'field_61c4a92f4c0ce'),
(247, 9, 'content_0_service_blocks_3_title', 'Historic Preservation'),
(248, 9, '_content_0_service_blocks_3_title', 'field_61c4a9264c0cd'),
(249, 9, 'content_0_service_blocks_3_text', '<ul>\r\n 	<li>conservation and documentation of heritage monuments</li>\r\n 	<li>reconstruction of monuments</li>\r\n 	<li>reconstruction of buildings</li>\r\n 	<li>projects of demolition</li>\r\n 	<li>timber frame and traditional construction</li>\r\n 	<li>maintenance and adaptation</li>\r\n</ul>'),
(250, 9, '_content_0_service_blocks_3_text', 'field_61c4a92f4c0ce'),
(251, 9, 'content_0_service_blocks', '4'),
(252, 9, '_content_0_service_blocks', 'field_61c4a8b44c0cc'),
(253, 9, 'content', 'a:1:{i:0;s:16:\"services_section\";}'),
(254, 9, '_content', 'field_61c44fc0720a9'),
(255, 67, 'content_0_service_blocks_0_title', 'Architecture'),
(256, 67, '_content_0_service_blocks_0_title', 'field_61c4a9264c0cd'),
(257, 67, 'content_0_service_blocks_0_text', '<ul>\r\n 	<li>projects of buildings and objects of all categories</li>\r\n 	<li>concepts, sketches, visualizations, renderings, 3d modelling</li>\r\n 	<li>architecture design</li>\r\n 	<li>executive projects</li>\r\n 	<li>details design/detail drawings</li>\r\n 	<li>interior design</li>\r\n</ul>'),
(258, 67, '_content_0_service_blocks_0_text', 'field_61c4a92f4c0ce'),
(259, 67, 'content_0_service_blocks_1_title', 'Engineering'),
(260, 67, '_content_0_service_blocks_1_title', 'field_61c4a9264c0cd'),
(261, 67, 'content_0_service_blocks_1_text', '<ul>\r\n 	<li>building construction systems design</li>\r\n 	<li>building information modeling (BIM)</li>\r\n 	<li>structural design</li>\r\n 	<li>static calculations</li>\r\n</ul>'),
(262, 67, '_content_0_service_blocks_1_text', 'field_61c4a92f4c0ce'),
(263, 67, 'content_0_service_blocks_2_title', 'Management'),
(264, 67, '_content_0_service_blocks_2_title', 'field_61c4a9264c0cd'),
(265, 67, 'content_0_service_blocks_2_text', '<ul>\r\n 	<li>preparation of technical documentation at all stages of design in cooperation with all involved design teams</li>\r\n 	<li>all administration, management coordination and supervision of the project</li>\r\n 	<li>coordination and support for construction process</li>\r\n 	<li>cooperation with internal and external specialists</li>\r\n 	<li>achieving building permissions</li>\r\n</ul>'),
(266, 67, '_content_0_service_blocks_2_text', 'field_61c4a92f4c0ce'),
(267, 67, 'content_0_service_blocks_3_title', 'Historic Preservation'),
(268, 67, '_content_0_service_blocks_3_title', 'field_61c4a9264c0cd'),
(269, 67, 'content_0_service_blocks_3_text', '<ul>\r\n 	<li>conservation and documentation of heritage monuments</li>\r\n 	<li>reconstruction of monuments</li>\r\n 	<li>reconstruction of buildings</li>\r\n 	<li>projects of demolition</li>\r\n 	<li>timber frame and traditional construction</li>\r\n 	<li>maintenance and adaptation</li>\r\n</ul>'),
(270, 67, '_content_0_service_blocks_3_text', 'field_61c4a92f4c0ce'),
(271, 67, 'content_0_service_blocks', '4'),
(272, 67, '_content_0_service_blocks', 'field_61c4a8b44c0cc'),
(273, 67, 'content', 'a:1:{i:0;s:16:\"services_section\";}'),
(274, 67, '_content', 'field_61c44fc0720a9'),
(275, 68, 'content_0_service_blocks_0_title', 'Architecture'),
(276, 68, '_content_0_service_blocks_0_title', 'field_61c4a9264c0cd'),
(277, 68, 'content_0_service_blocks_0_text', '<ul>\r\n 	<li>projects of buildings and objects of all categories</li>\r\n 	<li>concepts, sketches, visualizations, renderings, 3d modelling</li>\r\n 	<li>architecture design</li>\r\n 	<li>executive projects</li>\r\n 	<li>details design/detail drawings</li>\r\n 	<li>interior design</li>\r\n</ul>'),
(278, 68, '_content_0_service_blocks_0_text', 'field_61c4a92f4c0ce'),
(279, 68, 'content_0_service_blocks_1_title', 'Engineering'),
(280, 68, '_content_0_service_blocks_1_title', 'field_61c4a9264c0cd'),
(281, 68, 'content_0_service_blocks_1_text', '<ul>\r\n 	<li>building construction systems design</li>\r\n 	<li>building information modeling (BIM)</li>\r\n 	<li>structural design</li>\r\n 	<li>static calculations</li>\r\n</ul>'),
(282, 68, '_content_0_service_blocks_1_text', 'field_61c4a92f4c0ce'),
(283, 68, 'content_0_service_blocks_2_title', 'Management'),
(284, 68, '_content_0_service_blocks_2_title', 'field_61c4a9264c0cd'),
(285, 68, 'content_0_service_blocks_2_text', '<ul>\r\n 	<li>preparation of technical documentation at all stages of design\r\nin cooperation with all involved design teams</li>\r\n 	<li>all administration, management coordination and supervision\r\nof the project</li>\r\n 	<li>coordination and support for construction process</li>\r\n 	<li>cooperation with internal and external specialists</li>\r\n 	<li>achieving building permissions</li>\r\n</ul>'),
(286, 68, '_content_0_service_blocks_2_text', 'field_61c4a92f4c0ce'),
(287, 68, 'content_0_service_blocks_3_title', 'Historic Preservation'),
(288, 68, '_content_0_service_blocks_3_title', 'field_61c4a9264c0cd'),
(289, 68, 'content_0_service_blocks_3_text', '<ul>\r\n 	<li>conservation and documentation of heritage monuments</li>\r\n 	<li>reconstruction of monuments</li>\r\n 	<li>reconstruction of buildings</li>\r\n 	<li>projects of demolition</li>\r\n 	<li>timber frame and traditional construction</li>\r\n 	<li>maintenance and adaptation</li>\r\n</ul>'),
(290, 68, '_content_0_service_blocks_3_text', 'field_61c4a92f4c0ce'),
(291, 68, 'content_0_service_blocks', '4'),
(292, 68, '_content_0_service_blocks', 'field_61c4a8b44c0cc'),
(293, 68, 'content', 'a:1:{i:0;s:16:\"services_section\";}'),
(294, 68, '_content', 'field_61c44fc0720a9'),
(295, 69, '_wp_attached_file', '2021/12/service-1.jpg'),
(296, 69, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1490;s:4:\"file\";s:21:\"2021/12/service-1.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-1-300x226.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:226;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-1-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-1-768x578.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:578;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-1-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-1-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-1-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-1-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(297, 37, '_thumbnail_id', '156'),
(298, 70, '_edit_last', '1'),
(299, 70, '_edit_lock', '1641211192:1'),
(300, 71, '_wp_attached_file', '2021/12/service-2.jpg'),
(301, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1489;s:4:\"file\";s:21:\"2021/12/service-2.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-2-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-2-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-2-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-2-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-2-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-2-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-2-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(302, 70, '_thumbnail_id', '146'),
(303, 70, 'gallery', ''),
(304, 70, '_gallery', 'field_61c450eee6ced'),
(305, 70, 'type', 'Cabin'),
(306, 70, '_type', 'field_61c4512fe6cee'),
(307, 70, 'location', 'Lille Sommarøya island, Norway'),
(308, 70, '_location', 'field_61c45136e6cef'),
(309, 70, 'year', '2019'),
(310, 70, '_year', 'field_61c4513ee6cf0'),
(311, 70, 'description', 'Private cabin designed on lille sommaroy island with private beach and pure nature not touched by human hand. Only neighbors are the sea, sky and breathtaking sunsets.'),
(312, 70, '_description', 'field_61c45150e6cf1'),
(313, 72, '_edit_last', '1'),
(314, 72, '_edit_lock', '1641211254:1'),
(315, 73, '_wp_attached_file', '2021/12/service-3.jpg'),
(316, 73, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1489;s:4:\"file\";s:21:\"2021/12/service-3.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-3-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-3-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-3-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-3-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-3-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-3-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-3-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(317, 72, '_thumbnail_id', '147'),
(318, 72, 'gallery', ''),
(319, 72, '_gallery', 'field_61c450eee6ced'),
(320, 72, 'type', 'Single family house'),
(321, 72, '_type', 'field_61c4512fe6cee'),
(322, 72, 'location', 'Tromsdalen, Tromsø, Norway'),
(323, 72, '_location', 'field_61c45136e6cef'),
(324, 72, 'year', '2019'),
(325, 72, '_year', 'field_61c4513ee6cf0'),
(326, 72, 'description', 'Modern villa is designed on a very difficult triangular plot on the east side of tromsdalen. The Area of the house is 500 square meters. From the villa you can enjoy a view over the whole Tromsø in east and south direction and pleasure your soul with magic sunsets, aurora light and nightlights of north capital.'),
(327, 72, '_description', 'field_61c45150e6cf1'),
(328, 74, '_edit_last', '1'),
(329, 74, '_edit_lock', '1641211279:1'),
(330, 75, '_wp_attached_file', '2021/12/service-4.jpg'),
(331, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1489;s:4:\"file\";s:21:\"2021/12/service-4.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-4-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-4-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-4-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-4-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-4-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-4-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-4-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(332, 74, '_thumbnail_id', '149'),
(333, 74, 'gallery', ''),
(334, 74, '_gallery', 'field_61c450eee6ced'),
(335, 74, 'type', 'Single family house'),
(336, 74, '_type', 'field_61c4512fe6cee'),
(337, 74, 'location', 'Tromsdalen, Tromsø, Norway'),
(338, 74, '_location', 'field_61c45136e6cef'),
(339, 74, 'year', '2019'),
(340, 74, '_year', 'field_61c4513ee6cf0'),
(341, 74, 'description', 'Four modern apartments in two houses located in tromsdalen near to Tromsø arctic cathedral. Architecture of houses is very minimalistic and functional adjusted to city life requirements of young families who were targeted customer'),
(342, 74, '_description', 'field_61c45150e6cf1'),
(343, 76, '_edit_last', '1'),
(344, 76, '_edit_lock', '1641211316:1'),
(345, 77, '_wp_attached_file', '2021/12/service-5.jpg'),
(346, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1489;s:4:\"file\";s:21:\"2021/12/service-5.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-5-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-5-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-5-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-5-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-5-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-5-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-5-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(347, 76, '_thumbnail_id', '150'),
(348, 76, 'gallery', ''),
(349, 76, '_gallery', 'field_61c450eee6ced'),
(350, 76, 'type', 'Single family house'),
(351, 76, '_type', 'field_61c4512fe6cee'),
(352, 76, 'location', 'Tromsdalen, Tromsø, Norway'),
(353, 76, '_location', 'field_61c45136e6cef'),
(354, 76, 'year', '2019'),
(355, 76, '_year', 'field_61c4513ee6cf0'),
(356, 76, 'description', 'Modern villa with an area of 700 square meters. Designed on the east side of Tromsdalen. From willa you can enjoy the view over the whole Tromsø in east and south direction and pleasure your soul with magic sunsets.'),
(357, 76, '_description', 'field_61c45150e6cf1'),
(358, 78, '_edit_last', '1'),
(359, 78, '_edit_lock', '1641211357:1'),
(360, 79, '_wp_attached_file', '2021/12/service-6.jpg'),
(361, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1489;s:4:\"file\";s:21:\"2021/12/service-6.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-6-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-6-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-6-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-6-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-6-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-6-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-6-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(362, 78, '_thumbnail_id', '151'),
(363, 78, 'gallery', ''),
(364, 78, '_gallery', 'field_61c450eee6ced'),
(365, 78, 'type', 'Emergency/ambulance station\r\nParking building\r\n Electrical busses base'),
(366, 78, '_type', 'field_61c4512fe6cee'),
(367, 78, 'location', 'Tromsø, Norway'),
(368, 78, '_location', 'field_61c45136e6cef'),
(369, 78, 'year', '2019'),
(370, 78, '_year', 'field_61c4513ee6cf0'),
(371, 78, 'description', 'Project contains the design of a new ambulance base connected with the landing field of an ambulance helicopter located on an existing old emergency station next to Tromsø hospital. Lower floors contain parking lots. On the ground floor will be located a modern base and charging station for Tromsø new electrical buses.'),
(372, 78, '_description', 'field_61c45150e6cf1'),
(373, 80, '_edit_last', '1'),
(374, 80, '_edit_lock', '1641211401:1'),
(375, 81, '_wp_attached_file', '2021/12/service-7.jpg'),
(376, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1489;s:4:\"file\";s:21:\"2021/12/service-7.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-7-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-7-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-7-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-7-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-7-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-7-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-7-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-7-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(377, 80, '_thumbnail_id', '152'),
(378, 80, 'gallery', ''),
(379, 80, '_gallery', 'field_61c450eee6ced'),
(380, 80, 'type', 'Single family house'),
(381, 80, '_type', 'field_61c4512fe6cee'),
(382, 80, 'location', 'Tromsø, Norway'),
(383, 80, '_location', 'field_61c45136e6cef'),
(384, 80, 'year', '2019'),
(385, 80, '_year', 'field_61c4513ee6cf0'),
(386, 80, 'description', 'Three modern single family houses designed in one of most attractive localisations on the island — Norrøna on the west side of Tromsøya. Homes got a minimalistic architectural design with large glass surfaces and a subtle choice of materials. Here you can live in quiet surroundings while having “all” desired facilities within walking distance of the front door; school, kindergartens, recreation areas, light trail and shopping park. Good connections to both the city center and pure nature.'),
(387, 80, '_description', 'field_61c45150e6cf1'),
(388, 82, '_edit_last', '1'),
(389, 82, '_edit_lock', '1641211408:1'),
(390, 83, '_wp_attached_file', '2021/12/service-8.jpg'),
(391, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1489;s:4:\"file\";s:21:\"2021/12/service-8.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-8-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-8-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-8-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-8-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-8-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-8-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-8-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-8-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(392, 82, '_thumbnail_id', '145'),
(393, 82, 'gallery', ''),
(394, 82, '_gallery', 'field_61c450eee6ced'),
(395, 82, 'type', 'Single family house'),
(396, 82, '_type', 'field_61c4512fe6cee'),
(397, 82, 'location', 'Tromsø, Norway'),
(398, 82, '_location', 'field_61c45136e6cef'),
(399, 82, 'year', '2019'),
(400, 82, '_year', 'field_61c4513ee6cf0'),
(401, 82, 'description', 'Task was to design modern building with respect to all historicall values of proteced area. Also very important was to shape architecture in harmony to surrounding buildings which presents different architecture styles , different forms and were builded at different times of past history. Each past time period leaved sample of typical, current for its period architecture in area and that is why architects have choosen to follow this pattern and design building which is actual for our times , minimalistic and modern interpretation and translation of local, traditional architecture.\r\n\r\nDesign fits skolegatas linear (exactly linear along the road) historic hierarchy and our buildings ends some kind of time/road axis. We have respected surrounding buildings form, roof shape , proportions and localisation against acces road, against terrain shape etc.\r\n\r\nSome of building details like takoplet are in meaning of architect modern interpretation of hisotric roof bays visible on samples above as wel as historical accented and overbuild entrances have been repeated in desig by accenting entrance and protecting it by sunking it into basement frontal part.\r\n\r\nProportions and size have been choosen to fit tomt shape, to be in harmony with neighbour buildings and to not be dominant in area.\r\n\r\nAlso all building parameters like gesims and mone height has been designed within KPA regulations\r\n\r\nAll protected and existing buildings are kept as they are and new building has no impact on protected architecture besides removing part of secondary not original extension at skolegata 52. In place of this extension garage has been designed which creates kind of connection and unity between postwar typical for its period architecture with new modern architecture. On top of this garage unit which crosses border and connects both buildings architects placed quite big terrace which provides air space and distance between buidlings what have meaning in live quality and visual perception of buidlings in tromsos panorama and skolegatas panorama.\r\n\r\nplot shape and rhytm of street required rectangular shape and aligned with existing buidlings localisation. Designed building is divided in middle into two living parts. Beacuse of that northern del/apartment has been deprived southern sun. To ensure the same libving quality for both parts terrace for northern part has been designed on top of free standing storages. Acces to this terrace is thru hanging in the air stairs connected with house. Exit to this part has been designed by breaking calm architecture with creating angled dynamic triangular extension directed north east coovered with glass.\r\n\r\nAlso some angled accents has been designed in southern part of buidling providing more dynamic feeling of architecture and adjusting terrace space. Also building can be feel more light thanks to these details.\r\n\r\nOutdoor are is designed in back part of the plot as continuation of neighbour solutions. This area covers 125sq meters what fullfil requirements and extends in its 6m width along western plot border and along western facade\r\n\r\nacces to this sone has been provided for both living units.\r\n\r\nSuthern and eastern facade is equipped with huge glass surfaces providing amazing panorama view. Also building is transparent , light , not heavy and not dominant in area thanks to its proportions between glass and natural wood planks on facade. It creates feeeling of harmony with nature and perfectly fits in hill shape of plot/area. Material choice-wooden facade and glass creates unity with plot/hill green background providing new attractive element in skolegatas frontage panorama.\r\n\r\nRoof is designed as steel standing steam grey colour gable roof as natural continuation of area roof angles/shapes/types.\r\n\r\nbuilding was designed with respect for historicall architecture, fullfiling all reguleringsplan and KPA requirements. Proportions are correct and not overwhelming. Design is correct for current times in its form and in harmony with surrounding environment and smoothly fits into protected area. Also internal design and parameters makes this project user friendly and ensures high living standard in city centrum for future residents.'),
(402, 82, '_description', 'field_61c45150e6cf1'),
(403, 84, '_edit_last', '1'),
(404, 84, '_edit_lock', '1641211484:1'),
(405, 85, '_wp_attached_file', '2021/12/service-9.jpg'),
(406, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1981;s:6:\"height\";i:1489;s:4:\"file\";s:21:\"2021/12/service-9.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"service-9-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"service-9-1024x770.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:770;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"service-9-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"service-9-768x577.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:577;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:23:\"service-9-1536x1155.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-9-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-9-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:21:\"service-9-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(407, 84, '_thumbnail_id', '153'),
(408, 84, 'gallery', ''),
(409, 84, '_gallery', 'field_61c450eee6ced'),
(410, 84, 'type', 'Single family house'),
(411, 84, '_type', 'field_61c4512fe6cee'),
(412, 84, 'location', 'Tromsø, Norway'),
(413, 84, '_location', 'field_61c45136e6cef'),
(414, 84, 'year', '2019'),
(415, 84, '_year', 'field_61c4513ee6cf0'),
(416, 84, 'description', 'Modern villa located in the northern part of Tromsø island. Designed in postmodern style inspired by European and American modernism architecture of the early 20th century. Areal 600 square meters. Villa has a magnificent panorama view towards east and south providing breathtaking view onto sunset around Tromsø, and onto aurora lights at night.'),
(417, 84, '_description', 'field_61c45150e6cf1'),
(418, 86, '_edit_last', '1'),
(419, 86, '_edit_lock', '1641211524:1'),
(420, 87, '_wp_attached_file', '2021/12/service-10.jpg'),
(421, 87, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1291;s:6:\"height\";i:878;s:4:\"file\";s:22:\"2021/12/service-10.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"service-10-300x204.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:204;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"service-10-1024x696.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:696;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"service-10-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"service-10-768x522.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:522;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:22:\"service-10-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:22:\"service-10-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:22:\"service-10-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(422, 86, '_thumbnail_id', '154'),
(423, 86, 'gallery', ''),
(424, 86, '_gallery', 'field_61c450eee6ced'),
(425, 86, 'type', ''),
(426, 86, '_type', 'field_61c4512fe6cee'),
(427, 86, 'location', ''),
(428, 86, '_location', 'field_61c45136e6cef'),
(429, 86, 'year', ''),
(430, 86, '_year', 'field_61c4513ee6cf0'),
(431, 86, 'description', ''),
(432, 86, '_description', 'field_61c45150e6cf1'),
(433, 5, '_edit_last', '1'),
(434, 5, 'content_0_choose_projects_to_show', 'a:9:{i:0;s:2:\"37\";i:1;s:2:\"70\";i:2;s:2:\"72\";i:3;s:2:\"74\";i:4;s:2:\"76\";i:5;s:2:\"78\";i:6;s:2:\"80\";i:7;s:2:\"82\";i:8;s:2:\"84\";}'),
(435, 5, '_content_0_choose_projects_to_show', 'field_61c4509aeea46'),
(436, 5, 'content', 'a:1:{i:0;s:23:\"projects_slider_section\";}'),
(437, 5, '_content', 'field_61c44fc0720a9');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(438, 88, 'content_0_choose_projects_to_show', 'a:9:{i:0;s:2:\"37\";i:1;s:2:\"70\";i:2;s:2:\"72\";i:3;s:2:\"74\";i:4;s:2:\"76\";i:5;s:2:\"78\";i:6;s:2:\"80\";i:7;s:2:\"82\";i:8;s:2:\"84\";}'),
(439, 88, '_content_0_choose_projects_to_show', 'field_61c4509aeea46'),
(440, 88, 'content', 'a:1:{i:0;s:23:\"projects_slider_section\";}'),
(441, 88, '_content', 'field_61c44fc0720a9'),
(442, 89, '_menu_item_type', 'post_type_archive'),
(443, 89, '_menu_item_menu_item_parent', '0'),
(444, 89, '_menu_item_object_id', '-21'),
(445, 89, '_menu_item_object', 'project'),
(446, 89, '_menu_item_target', ''),
(447, 89, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(448, 89, '_menu_item_xfn', ''),
(449, 89, '_menu_item_url', ''),
(451, 90, '_menu_item_type', 'taxonomy'),
(452, 90, '_menu_item_menu_item_parent', '0'),
(453, 90, '_menu_item_object_id', '9'),
(454, 90, '_menu_item_object', 'projects-categories'),
(455, 90, '_menu_item_target', ''),
(456, 90, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(457, 90, '_menu_item_xfn', ''),
(458, 90, '_menu_item_url', ''),
(460, 91, '_menu_item_type', 'taxonomy'),
(461, 91, '_menu_item_menu_item_parent', '0'),
(462, 91, '_menu_item_object_id', '11'),
(463, 91, '_menu_item_object', 'projects-categories'),
(464, 91, '_menu_item_target', ''),
(465, 91, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(466, 91, '_menu_item_xfn', ''),
(467, 91, '_menu_item_url', ''),
(469, 92, '_menu_item_type', 'taxonomy'),
(470, 92, '_menu_item_menu_item_parent', '0'),
(471, 92, '_menu_item_object_id', '13'),
(472, 92, '_menu_item_object', 'projects-categories'),
(473, 92, '_menu_item_target', ''),
(474, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(475, 92, '_menu_item_xfn', ''),
(476, 92, '_menu_item_url', ''),
(478, 93, '_menu_item_type', 'taxonomy'),
(479, 93, '_menu_item_menu_item_parent', '0'),
(480, 93, '_menu_item_object_id', '15'),
(481, 93, '_menu_item_object', 'projects-categories'),
(482, 93, '_menu_item_target', ''),
(483, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(484, 93, '_menu_item_xfn', ''),
(485, 93, '_menu_item_url', ''),
(487, 94, '_menu_item_type', 'taxonomy'),
(488, 94, '_menu_item_menu_item_parent', '0'),
(489, 94, '_menu_item_object_id', '17'),
(490, 94, '_menu_item_object', 'projects-categories'),
(491, 94, '_menu_item_target', ''),
(492, 94, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(493, 94, '_menu_item_xfn', ''),
(494, 94, '_menu_item_url', ''),
(496, 95, '_menu_item_type', 'taxonomy'),
(497, 95, '_menu_item_menu_item_parent', '0'),
(498, 95, '_menu_item_object_id', '19'),
(499, 95, '_menu_item_object', 'projects-categories'),
(500, 95, '_menu_item_target', ''),
(501, 95, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(502, 95, '_menu_item_xfn', ''),
(503, 95, '_menu_item_url', ''),
(505, 96, '_menu_item_type', 'taxonomy'),
(506, 96, '_menu_item_menu_item_parent', '0'),
(507, 96, '_menu_item_object_id', '21'),
(508, 96, '_menu_item_object', 'projects-categories'),
(509, 96, '_menu_item_target', ''),
(510, 96, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(511, 96, '_menu_item_xfn', ''),
(512, 96, '_menu_item_url', ''),
(514, 97, '_menu_item_type', 'taxonomy'),
(515, 97, '_menu_item_menu_item_parent', '0'),
(516, 97, '_menu_item_object_id', '23'),
(517, 97, '_menu_item_object', 'projects-categories'),
(518, 97, '_menu_item_target', ''),
(519, 97, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(520, 97, '_menu_item_xfn', ''),
(521, 97, '_menu_item_url', ''),
(913, 145, '_wp_attached_file', '2021/12/S54-15-scaled.jpg'),
(914, 145, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1707;s:4:\"file\";s:25:\"2021/12/S54-15-scaled.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"S54-15-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"S54-15-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"S54-15-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"S54-15-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:20:\"S54-15-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:20:\"S54-15-2048x1365.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1365;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:18:\"S54-15-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:18:\"S54-15-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:18:\"S54-15-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:10:\"S54-15.jpg\";}'),
(915, 146, '_wp_attached_file', '2021/12/LS-1-scaled.jpg'),
(916, 146, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1280;s:4:\"file\";s:23:\"2021/12/LS-1-scaled.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"LS-1-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"LS-1-1024x512.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"LS-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"LS-1-768x384.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:17:\"LS-1-1536x768.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:18:\"LS-1-2048x1024.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:16:\"LS-1-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:16:\"LS-1-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:16:\"LS-1-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:8:\"LS-1.jpg\";}'),
(917, 147, '_wp_attached_file', '2021/12/M2-3.jpg'),
(918, 147, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2250;s:6:\"height\";i:1125;s:4:\"file\";s:16:\"2021/12/M2-3.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"M2-3-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"M2-3-1024x512.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"M2-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"M2-3-768x384.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:17:\"M2-3-1536x768.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:18:\"M2-3-2048x1024.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:16:\"M2-3-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:16:\"M2-3-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:16:\"M2-3-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(919, 149, '_wp_attached_file', '2021/12/T16-8.jpg'),
(920, 149, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2250;s:6:\"height\";i:1125;s:4:\"file\";s:17:\"2021/12/T16-8.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"T16-8-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"T16-8-1024x512.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"T16-8-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"T16-8-768x384.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:18:\"T16-8-1536x768.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:19:\"T16-8-2048x1024.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:17:\"T16-8-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:17:\"T16-8-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:17:\"T16-8-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(921, 150, '_wp_attached_file', '2021/12/G35-1.jpg'),
(922, 150, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1000;s:4:\"file\";s:17:\"2021/12/G35-1.jpg\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"G35-1-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"G35-1-1024x512.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"G35-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"G35-1-768x384.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:18:\"G35-1-1536x768.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:17:\"G35-1-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:17:\"G35-1-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:17:\"G35-1-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(923, 151, '_wp_attached_file', '2021/12/G12-11-scaled.jpg'),
(924, 151, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1280;s:4:\"file\";s:25:\"2021/12/G12-11-scaled.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"G12-11-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"G12-11-1024x512.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"G12-11-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"G12-11-768x384.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"G12-11-1536x768.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:20:\"G12-11-2048x1024.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:18:\"G12-11-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:18:\"G12-11-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:18:\"G12-11-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:10:\"G12-11.jpg\";}'),
(925, 152, '_wp_attached_file', '2021/12/B12-3-scaled.jpg'),
(926, 152, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1705;s:4:\"file\";s:24:\"2021/12/B12-3-scaled.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"B12-3-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"B12-3-1024x682.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:682;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"B12-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"B12-3-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"B12-3-1536x1023.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1023;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:19:\"B12-3-2048x1364.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1364;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:17:\"B12-3-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:17:\"B12-3-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:17:\"B12-3-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:9:\"B12-3.jpg\";}'),
(927, 153, '_wp_attached_file', '2021/12/K26-4-scaled.jpg'),
(928, 153, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:2560;s:6:\"height\";i:1408;s:4:\"file\";s:24:\"2021/12/K26-4-scaled.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"K26-4-300x165.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:165;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"K26-4-1024x563.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:563;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"K26-4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"K26-4-768x422.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:422;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:18:\"K26-4-1536x845.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:845;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:19:\"K26-4-2048x1126.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1126;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:17:\"K26-4-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:17:\"K26-4-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:17:\"K26-4-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:9:\"K26-4.jpg\";}'),
(929, 154, '_wp_attached_file', '2021/12/A11-1.jpg'),
(930, 154, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2400;s:6:\"height\";i:1200;s:4:\"file\";s:17:\"2021/12/A11-1.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"A11-1-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"A11-1-1024x512.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"A11-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"A11-1-768x384.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:18:\"A11-1-1536x768.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:19:\"A11-1-2048x1024.jpg\";s:5:\"width\";i:2048;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:17:\"A11-1-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:17:\"A11-1-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:17:\"A11-1-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(931, 156, '_wp_attached_file', '2021/12/H8A-20-scaled.jpg'),
(932, 156, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1920;s:6:\"height\";i:2560;s:4:\"file\";s:25:\"2021/12/H8A-20-scaled.jpg\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"H8A-20-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"H8A-20-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"H8A-20-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"H8A-20-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:20:\"H8A-20-1152x1536.jpg\";s:5:\"width\";i:1152;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"2048x2048\";a:4:{s:4:\"file\";s:20:\"H8A-20-1536x2048.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:2048;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"service-thumbnails\";a:4:{s:4:\"file\";s:18:\"H8A-20-360x160.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:160;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"new-thumbnails\";a:4:{s:4:\"file\";s:18:\"H8A-20-360x240.jpg\";s:5:\"width\";i:360;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:18:\"product-thumbnails\";a:4:{s:4:\"file\";s:18:\"H8A-20-848x436.jpg\";s:5:\"width\";i:848;s:6:\"height\";i:436;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:14:\"original_image\";s:10:\"H8A-20.jpg\";}'),
(1023, 157, 'content_0_service_blocks_0_title', 'Architecture'),
(1024, 157, '_content_0_service_blocks_0_title', 'field_61c4a9264c0cd'),
(1025, 157, 'content_0_service_blocks_0_text', '<ul>\r\n 	<li>projects of buildings and objects of all categories</li>\r\n 	<li>concepts, sketches, visualizations, renderings, 3d modelling</li>\r\n 	<li>architecture design</li>\r\n 	<li>executive projects</li>\r\n 	<li>details design/detail drawings</li>\r\n 	<li>interior design</li>\r\n</ul>'),
(1026, 157, '_content_0_service_blocks_0_text', 'field_61c4a92f4c0ce'),
(1027, 157, 'content_0_service_blocks_1_title', 'Engineering'),
(1028, 157, '_content_0_service_blocks_1_title', 'field_61c4a9264c0cd'),
(1029, 157, 'content_0_service_blocks_1_text', '<ul>\r\n 	<li>building construction systems design</li>\r\n 	<li>building information modeling (BIM)</li>\r\n 	<li>structural design</li>\r\n 	<li>static calculations</li>\r\n</ul>'),
(1030, 157, '_content_0_service_blocks_1_text', 'field_61c4a92f4c0ce'),
(1031, 157, 'content_0_service_blocks_2_title', 'Management'),
(1032, 157, '_content_0_service_blocks_2_title', 'field_61c4a9264c0cd'),
(1033, 157, 'content_0_service_blocks_2_text', '<ul>\r\n 	<li>preparation of technical documentation at all stages of design\r\nin cooperation with all involved design teams</li>\r\n 	<li>all administration, management coordination and supervision\r\nof the project</li>\r\n 	<li>coordination and support for construction process</li>\r\n 	<li>cooperation with internal and external specialists</li>\r\n 	<li>achieving building permissions</li>\r\n</ul>'),
(1034, 157, '_content_0_service_blocks_2_text', 'field_61c4a92f4c0ce'),
(1035, 157, 'content_0_service_blocks_3_title', 'Historic Preservation'),
(1036, 157, '_content_0_service_blocks_3_title', 'field_61c4a9264c0cd'),
(1037, 157, 'content_0_service_blocks_3_text', '<ul>\r\n 	<li>conservation and documentation of heritage monuments</li>\r\n 	<li>reconstruction of monuments</li>\r\n 	<li>reconstruction of buildings</li>\r\n 	<li>projects of demolition</li>\r\n 	<li>timber frame and traditional construction</li>\r\n 	<li>maintenance and adaptation</li>\r\n</ul>'),
(1038, 157, '_content_0_service_blocks_3_text', 'field_61c4a92f4c0ce'),
(1039, 157, 'content_0_service_blocks', '4'),
(1040, 157, '_content_0_service_blocks', 'field_61c4a8b44c0cc'),
(1041, 157, 'content', 'a:1:{i:0;s:16:\"services_section\";}'),
(1042, 157, '_content', 'field_61c44fc0720a9'),
(1043, 157, '_dp_original', '9'),
(1044, 157, '_edit_lock', '1641213928:1'),
(1045, 158, 'content_0_service_blocks_0_title', 'Architecture'),
(1046, 158, '_content_0_service_blocks_0_title', 'field_61c4a9264c0cd'),
(1047, 158, 'content_0_service_blocks_0_text', '<ul>\r\n 	<li>projects of buildings and objects of all categories</li>\r\n 	<li>concepts, sketches, visualizations, renderings, 3d modelling</li>\r\n 	<li>architecture design</li>\r\n 	<li>executive projects</li>\r\n 	<li>details design/detail drawings</li>\r\n 	<li>interior design</li>\r\n</ul>'),
(1048, 158, '_content_0_service_blocks_0_text', 'field_61c4a92f4c0ce'),
(1049, 158, 'content_0_service_blocks_1_title', 'Engineering'),
(1050, 158, '_content_0_service_blocks_1_title', 'field_61c4a9264c0cd'),
(1051, 158, 'content_0_service_blocks_1_text', '<ul>\r\n 	<li>building construction systems design</li>\r\n 	<li>building information modeling (BIM)</li>\r\n 	<li>structural design</li>\r\n 	<li>static calculations</li>\r\n</ul>'),
(1052, 158, '_content_0_service_blocks_1_text', 'field_61c4a92f4c0ce'),
(1053, 158, 'content_0_service_blocks_2_title', 'Management'),
(1054, 158, '_content_0_service_blocks_2_title', 'field_61c4a9264c0cd'),
(1055, 158, 'content_0_service_blocks_2_text', '<ul>\r\n 	<li>preparation of technical documentation at all stages of design in cooperation with all involved design teams</li>\r\n 	<li>all administration, management coordination and supervision of the project</li>\r\n 	<li>coordination and support for construction process</li>\r\n 	<li>cooperation with internal and external specialists</li>\r\n 	<li>achieving building permissions</li>\r\n</ul>'),
(1056, 158, '_content_0_service_blocks_2_text', 'field_61c4a92f4c0ce'),
(1057, 158, 'content_0_service_blocks_3_title', 'Historic Preservation'),
(1058, 158, '_content_0_service_blocks_3_title', 'field_61c4a9264c0cd'),
(1059, 158, 'content_0_service_blocks_3_text', '<ul>\r\n 	<li>conservation and documentation of heritage monuments</li>\r\n 	<li>reconstruction of monuments</li>\r\n 	<li>reconstruction of buildings</li>\r\n 	<li>projects of demolition</li>\r\n 	<li>timber frame and traditional construction</li>\r\n 	<li>maintenance and adaptation</li>\r\n</ul>'),
(1060, 158, '_content_0_service_blocks_3_text', 'field_61c4a92f4c0ce'),
(1061, 158, 'content_0_service_blocks', '4'),
(1062, 158, '_content_0_service_blocks', 'field_61c4a8b44c0cc'),
(1063, 158, 'content', 'a:1:{i:0;s:16:\"services_section\";}'),
(1064, 158, '_content', 'field_61c44fc0720a9');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-12-23 10:20:18', '2021-12-23 10:20:18', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2021-12-23 10:20:18', '2021-12-23 10:20:18', '', 0, 'http://pro-arkitektur-wp/build/?p=1', 0, 'post', '', 1),
(3, 1, '2021-12-23 10:20:18', '2021-12-23 10:20:18', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Our website address is: http://pro-arkitektur-wp/build.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Comments</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Media</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Embedded content from other websites</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you request a password reset, your IP address will be included in the reset email.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Suggested text: </strong>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-12-23 10:20:18', '2021-12-23 10:20:18', '', 0, 'http://pro-arkitektur-wp/build/?page_id=3', 0, 'page', '', 0),
(5, 1, '2021-12-23 10:24:21', '2021-12-23 10:24:21', '', 'Home page', '', 'publish', 'closed', 'closed', '', 'home-page', '', '', '2021-12-27 12:42:31', '2021-12-27 12:42:31', '', 0, 'http://pro-arkitektur-wp/build/?page_id=5', 0, 'page', '', 0),
(6, 1, '2021-12-23 10:24:21', '2021-12-23 10:24:21', '', 'Home page', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2021-12-23 10:24:21', '2021-12-23 10:24:21', '', 5, 'http://pro-arkitektur-wp/build/?p=6', 0, 'revision', '', 0),
(7, 1, '2021-12-23 10:24:36', '2021-12-23 10:24:36', '', 'Contacts', '', 'publish', 'closed', 'closed', '', 'contacts', '', '', '2021-12-23 16:46:09', '2021-12-23 16:46:09', '', 0, 'http://pro-arkitektur-wp/build/?page_id=7', 0, 'page', '', 0),
(8, 1, '2021-12-23 10:24:36', '2021-12-23 10:24:36', '', 'Contacts', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-12-23 10:24:36', '2021-12-23 10:24:36', '', 7, 'http://pro-arkitektur-wp/build/?p=8', 0, 'revision', '', 0),
(9, 1, '2021-12-23 10:24:48', '2021-12-23 10:24:48', '', 'Services', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2022-01-03 12:45:44', '2022-01-03 12:45:44', '', 0, 'http://pro-arkitektur-wp/build/?page_id=9', 0, 'page', '', 0),
(10, 1, '2021-12-23 10:24:48', '2021-12-23 10:24:48', '', 'Services', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2021-12-23 10:24:48', '2021-12-23 10:24:48', '', 9, 'http://pro-arkitektur-wp/build/?p=10', 0, 'revision', '', 0),
(12, 1, '2021-12-23 10:27:40', '2021-12-23 10:27:40', '<label> Your name\n    [text* your-name] </label>\n\n<label> Your email\n    [email* your-email] </label>\n\n<label> Subject\n    [text* your-subject] </label>\n\n<label> Your message (optional)\n    [textarea your-message] </label>\n\n[submit \"Submit\"]\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@pro-arkitektur-wp>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\n[_site_admin_email]\nReply-To: [your-email]\n\n0\n0\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@pro-arkitektur-wp>\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\n[your-email]\nReply-To: [_site_admin_email]\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2021-12-23 10:27:40', '2021-12-23 10:27:40', '', 0, 'http://pro-arkitektur-wp/build/?post_type=wpcf7_contact_form&p=12', 0, 'wpcf7_contact_form', '', 0),
(13, 1, '2021-12-23 10:29:58', '2021-12-23 10:29:58', '', 'polylang_mo_2', '', 'private', 'closed', 'closed', '', 'polylang_mo_2', '', '', '2021-12-23 10:29:58', '2021-12-23 10:29:58', '', 0, 'http://pro-arkitektur-wp/build/?post_type=polylang_mo&p=13', 0, 'polylang_mo', '', 0),
(14, 1, '2021-12-23 10:29:59', '2021-12-23 10:29:59', '', 'polylang_mo_4', '', 'private', 'closed', 'closed', '', 'polylang_mo_4', '', '', '2021-12-23 10:29:59', '2021-12-23 10:29:59', '', 0, 'http://pro-arkitektur-wp/build/?post_type=polylang_mo&p=14', 0, 'polylang_mo', '', 0),
(15, 1, '2021-12-23 10:30:06', '2021-12-23 10:30:06', '', 'Home page', '', 'publish', 'closed', 'closed', '', 'home-page-norsk-nynorsk', '', '', '2021-12-23 11:15:36', '2021-12-23 11:15:36', '', 0, 'http://pro-arkitektur-wp/build/home-page-norsk-nynorsk/', 0, 'page', '', 0),
(16, 1, '2021-12-23 10:30:32', '2021-12-23 10:30:32', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:12:\"show_in_rest\";i:0;}', 'Page', 'page', 'publish', 'closed', 'closed', '', 'group_61c44fb961852', '', '', '2021-12-28 19:40:15', '2021-12-28 19:40:15', '', 0, 'http://pro-arkitektur-wp/build/?post_type=acf-field-group&#038;p=16', 0, 'acf-field-group', '', 0),
(17, 1, '2021-12-23 10:30:32', '2021-12-23 10:30:32', 'a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:3:{s:20:\"layout_61c44fc73b61b\";a:6:{s:3:\"key\";s:20:\"layout_61c44fc73b61b\";s:5:\"label\";s:23:\"Projects Slider Section\";s:4:\"name\";s:23:\"projects_slider_section\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_61c4a89f4c0cb\";a:6:{s:3:\"key\";s:20:\"layout_61c4a89f4c0cb\";s:5:\"label\";s:16:\"Services Section\";s:4:\"name\";s:16:\"services_section\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:20:\"layout_61c4a2c22489e\";a:6:{s:3:\"key\";s:20:\"layout_61c4a2c22489e\";s:5:\"label\";s:28:\"Map + Contacts Block Section\";s:4:\"name\";s:26:\"map_contacts_block_section\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:11:\"Add Content\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_61c44fc0720a9', '', '', '2021-12-23 16:53:41', '2021-12-23 16:53:41', '', 16, 'http://pro-arkitektur-wp/build/?post_type=acf-field&#038;p=17', 0, 'acf-field', '', 0),
(18, 1, '2021-12-23 10:34:27', '2021-12-23 10:34:27', 'a:12:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61c44fc73b61b\";s:9:\"post_type\";a:1:{i:0;s:7:\"project\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:13:\"return_format\";s:2:\"id\";s:2:\"ui\";i:1;}', 'Choose projects to show', 'choose_projects_to_show', 'publish', 'closed', 'closed', '', 'field_61c4509aeea46', '', '', '2021-12-23 10:34:27', '2021-12-23 10:34:27', '', 17, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=18', 0, 'acf-field', '', 0),
(19, 1, '2021-12-23 10:34:43', '2021-12-23 10:34:43', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"project\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:12:\"show_in_rest\";i:0;}', 'Project', 'project', 'publish', 'closed', 'closed', '', 'group_61c450bc6bf22', '', '', '2021-12-27 13:05:43', '2021-12-27 13:05:43', '', 0, 'http://pro-arkitektur-wp/build/?post_type=acf-field-group&#038;p=19', 0, 'acf-field-group', '', 0),
(21, 1, '2021-12-23 10:37:17', '2021-12-23 10:37:17', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Gallery', 'gallery', 'publish', 'closed', 'closed', '', 'field_61c450eee6ced', '', '', '2021-12-27 13:05:42', '2021-12-27 13:05:42', '', 19, 'http://pro-arkitektur-wp/build/?post_type=acf-field&#038;p=21', 0, 'acf-field', '', 0),
(22, 1, '2021-12-23 10:37:17', '2021-12-23 10:37:17', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Type', 'type', 'publish', 'closed', 'closed', '', 'field_61c4512fe6cee', '', '', '2021-12-27 12:35:54', '2021-12-27 12:35:54', '', 19, 'http://pro-arkitektur-wp/build/?post_type=acf-field&#038;p=22', 1, 'acf-field', '', 0),
(23, 1, '2021-12-23 10:37:17', '2021-12-23 10:37:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Location', 'location', 'publish', 'closed', 'closed', '', 'field_61c45136e6cef', '', '', '2021-12-23 10:37:17', '2021-12-23 10:37:17', '', 19, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=23', 2, 'acf-field', '', 0),
(24, 1, '2021-12-23 10:37:17', '2021-12-23 10:37:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Year', 'year', 'publish', 'closed', 'closed', '', 'field_61c4513ee6cf0', '', '', '2021-12-23 10:37:17', '2021-12-23 10:37:17', '', 19, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=24', 3, 'acf-field', '', 0),
(25, 1, '2021-12-23 10:37:17', '2021-12-23 10:37:17', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Description', 'description', 'publish', 'closed', 'closed', '', 'field_61c45150e6cf1', '', '', '2021-12-23 10:37:17', '2021-12-23 10:37:17', '', 19, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=25', 4, 'acf-field', '', 0),
(28, 1, '2021-12-23 10:38:31', '2021-12-23 10:38:31', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"theme-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:12:\"show_in_rest\";i:0;}', 'Theme Settings', 'theme-settings', 'publish', 'closed', 'closed', '', 'group_61c4519bc28b6', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 0, 'http://pro-arkitektur-wp/build/?post_type=acf-field-group&#038;p=28', 0, 'acf-field-group', '', 0),
(29, 1, '2021-12-23 11:12:34', '2021-12-23 11:12:34', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:4:\"left\";s:8:\"endpoint\";i:0;}', 'Header', 'header', 'publish', 'closed', 'closed', '', 'field_61c457fcefe25', '', '', '2021-12-23 11:12:34', '2021-12-23 11:12:34', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=29', 0, 'acf-field', '', 0),
(30, 1, '2021-12-23 11:12:34', '2021-12-23 11:12:34', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Logo', 'logo', 'publish', 'closed', 'closed', '', 'field_61c4580cefe26', '', '', '2021-12-23 11:12:34', '2021-12-23 11:12:34', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=30', 1, 'acf-field', '', 0),
(31, 1, '2021-12-23 11:12:34', '2021-12-23 11:12:34', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Logo Dark', 'logo_dark', 'publish', 'closed', 'closed', '', 'field_61c4581eefe27', '', '', '2021-12-23 11:12:34', '2021-12-23 11:12:34', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=31', 2, 'acf-field', '', 0),
(32, 1, '2021-12-23 11:12:34', '2021-12-23 11:12:34', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:4:\"left\";s:8:\"endpoint\";i:0;}', 'Footer', 'footer', 'publish', 'closed', 'closed', '', 'field_61c45856efe28', '', '', '2021-12-23 11:12:34', '2021-12-23 11:12:34', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=32', 3, 'acf-field', '', 0),
(33, 1, '2021-12-23 11:12:34', '2021-12-23 11:12:34', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Copyrights', 'copyrights', 'publish', 'closed', 'closed', '', 'field_61c45865efe29', '', '', '2021-12-23 11:12:34', '2021-12-23 11:12:34', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=33', 4, 'acf-field', '', 0),
(34, 1, '2021-12-23 11:13:35', '2021-12-23 11:13:35', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2021-12-23 11:13:35', '2021-12-23 11:13:35', '', 0, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/logo.svg', 0, 'attachment', 'image/svg+xml', 0),
(35, 1, '2021-12-23 11:13:44', '2021-12-23 11:13:44', '', 'logo-dark', '', 'inherit', 'open', 'closed', '', 'logo-dark', '', '', '2021-12-23 11:13:44', '2021-12-23 11:13:44', '', 0, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/logo-dark.svg', 0, 'attachment', 'image/svg+xml', 0),
(36, 1, '2021-12-23 11:15:35', '2021-12-23 11:15:35', '', 'Home page', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2021-12-23 11:15:35', '2021-12-23 11:15:35', '', 15, 'http://pro-arkitektur-wp/build/?p=36', 0, 'revision', '', 0),
(37, 1, '2021-12-28 09:20:50', '2021-12-28 09:20:50', '', 'H8A', '', 'publish', 'closed', 'closed', '', 'h8a', '', '', '2022-01-03 12:08:16', '2022-01-03 12:08:16', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=37', 0, 'project', '', 0),
(38, 1, '2021-12-23 11:36:38', '2021-12-23 11:36:38', 'Project information page', 'projects', '', 'publish', 'closed', 'closed', '', 'projects', '', '', '2021-12-23 11:36:38', '2021-12-23 11:36:38', '', 0, 'http://pro-arkitektur-wp/build/?p=38', 1, 'nav_menu_item', '', 0),
(39, 1, '2021-12-23 11:36:38', '2021-12-23 11:36:38', '', 'services', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2021-12-23 11:36:38', '2021-12-23 11:36:38', '', 0, 'http://pro-arkitektur-wp/build/?p=39', 2, 'nav_menu_item', '', 0),
(40, 1, '2021-12-23 11:36:38', '2021-12-23 11:36:38', '', 'contacts', '', 'publish', 'closed', 'closed', '', 'contacts', '', '', '2021-12-23 11:36:38', '2021-12-23 11:36:38', '', 0, 'http://pro-arkitektur-wp/build/?p=40', 3, 'nav_menu_item', '', 0),
(41, 1, '2021-12-23 16:32:35', '2021-12-23 16:32:35', 'a:8:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61c4a2c22489e\";s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Map', 'map', 'publish', 'closed', 'closed', '', 'field_61c4a2e02489f', '', '', '2021-12-23 16:32:35', '2021-12-23 16:32:35', '', 17, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=41', 0, 'acf-field', '', 0),
(42, 1, '2021-12-23 16:32:35', '2021-12-23 16:32:35', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Latitude', 'latitude', 'publish', 'closed', 'closed', '', 'field_61c4a2ed248a0', '', '', '2021-12-23 16:32:35', '2021-12-23 16:32:35', '', 41, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=42', 0, 'acf-field', '', 0),
(43, 1, '2021-12-23 16:32:35', '2021-12-23 16:32:35', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"30\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Longitude', 'longitude', 'publish', 'closed', 'closed', '', 'field_61c4a301248a1', '', '', '2021-12-23 16:32:35', '2021-12-23 16:32:35', '', 41, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=43', 1, 'acf-field', '', 0),
(44, 1, '2021-12-23 16:32:35', '2021-12-23 16:32:35', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Marker', 'marker', 'publish', 'closed', 'closed', '', 'field_61c4a30e248a2', '', '', '2021-12-23 16:32:35', '2021-12-23 16:32:35', '', 41, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=44', 2, 'acf-field', '', 0),
(45, 1, '2021-12-23 16:32:35', '2021-12-23 16:32:35', 'a:12:{s:4:\"type\";s:6:\"number\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:4:\"step\";s:0:\"\";}', 'Zoom', 'zoom', 'publish', 'closed', 'closed', '', 'field_61c4a326248a3', '', '', '2021-12-23 16:32:35', '2021-12-23 16:32:35', '', 41, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=45', 3, 'acf-field', '', 0),
(59, 1, '2021-12-23 16:42:27', '2021-12-23 16:42:27', '', 'marker', '', 'inherit', 'open', 'closed', '', 'marker', '', '', '2021-12-23 16:42:27', '2021-12-23 16:42:27', '', 7, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/marker.svg', 0, 'attachment', 'image/svg+xml', 0),
(60, 1, '2021-12-23 16:43:43', '2021-12-23 16:43:43', '', 'Contacts', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-12-23 16:43:43', '2021-12-23 16:43:43', '', 7, 'http://pro-arkitektur-wp/build/?p=60', 0, 'revision', '', 0),
(62, 1, '2021-12-23 16:44:41', '2021-12-23 16:44:41', '', 'Contacts', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-12-23 16:44:41', '2021-12-23 16:44:41', '', 7, 'http://pro-arkitektur-wp/build/?p=62', 0, 'revision', '', 0),
(63, 1, '2021-12-23 16:46:09', '2021-12-23 16:46:09', '', 'Contacts', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-12-23 16:46:09', '2021-12-23 16:46:09', '', 7, 'http://pro-arkitektur-wp/build/?p=63', 0, 'revision', '', 0),
(64, 1, '2021-12-23 16:53:41', '2021-12-23 16:53:41', 'a:11:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_61c4a89f4c0cb\";s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:9:\"Add Block\";}', 'Service blocks', 'service_blocks', 'publish', 'closed', 'closed', '', 'field_61c4a8b44c0cc', '', '', '2021-12-23 16:53:41', '2021-12-23 16:53:41', '', 17, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=64', 0, 'acf-field', '', 0),
(65, 1, '2021-12-23 16:53:41', '2021-12-23 16:53:41', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'title', 'publish', 'closed', 'closed', '', 'field_61c4a9264c0cd', '', '', '2021-12-23 16:53:41', '2021-12-23 16:53:41', '', 64, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=65', 0, 'acf-field', '', 0),
(66, 1, '2021-12-23 16:53:41', '2021-12-23 16:53:41', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Text', 'text', 'publish', 'closed', 'closed', '', 'field_61c4a92f4c0ce', '', '', '2021-12-23 16:53:41', '2021-12-23 16:53:41', '', 64, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=66', 1, 'acf-field', '', 0),
(67, 1, '2021-12-23 16:56:57', '2021-12-23 16:56:57', '', 'Services', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2021-12-23 16:56:57', '2021-12-23 16:56:57', '', 9, 'http://pro-arkitektur-wp/build/?p=67', 0, 'revision', '', 0),
(68, 1, '2021-12-23 21:11:14', '2021-12-23 21:11:14', '', 'Services', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2021-12-23 21:11:14', '2021-12-23 21:11:14', '', 9, 'http://pro-arkitektur-wp/build/?p=68', 0, 'revision', '', 0),
(69, 1, '2021-12-27 12:26:34', '2021-12-27 12:26:34', '', 'service-1', '', 'inherit', 'open', 'closed', '', 'service-1', '', '', '2021-12-27 12:26:34', '2021-12-27 12:26:34', '', 37, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(70, 1, '2021-12-27 12:30:21', '2021-12-27 12:30:21', '', 'ls', '', 'publish', 'closed', 'closed', '', 'ls', '', '', '2022-01-03 12:02:14', '2022-01-03 12:02:14', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=70', 0, 'project', '', 0),
(71, 1, '2021-12-27 12:29:49', '2021-12-27 12:29:49', '', 'service-2', '', 'inherit', 'open', 'closed', '', 'service-2', '', '', '2021-12-27 12:29:49', '2021-12-27 12:29:49', '', 70, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(72, 1, '2021-12-27 12:31:27', '2021-12-27 12:31:27', '', 'M2', '', 'publish', 'closed', 'closed', '', 'm2', '', '', '2022-01-03 12:03:13', '2022-01-03 12:03:13', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=72', 0, 'project', '', 0),
(73, 1, '2021-12-27 12:31:16', '2021-12-27 12:31:16', '', 'service-3', '', 'inherit', 'open', 'closed', '', 'service-3', '', '', '2021-12-27 12:31:16', '2021-12-27 12:31:16', '', 72, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(74, 1, '2021-12-27 12:32:27', '2021-12-27 12:32:27', '', 'T16', '', 'publish', 'closed', 'closed', '', 't16', '', '', '2022-01-03 12:03:37', '2022-01-03 12:03:37', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=74', 0, 'project', '', 0),
(75, 1, '2021-12-27 12:32:07', '2021-12-27 12:32:07', '', 'service-4', '', 'inherit', 'open', 'closed', '', 'service-4', '', '', '2021-12-27 12:32:07', '2021-12-27 12:32:07', '', 74, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(76, 1, '2021-12-27 12:33:23', '2021-12-27 12:33:23', '', 'g35', '', 'publish', 'closed', 'closed', '', 'g35', '', '', '2022-01-03 12:04:12', '2022-01-03 12:04:12', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=76', 0, 'project', '', 0),
(77, 1, '2021-12-27 12:33:02', '2021-12-27 12:33:02', '', 'service-5', '', 'inherit', 'open', 'closed', '', 'service-5', '', '', '2021-12-27 12:33:02', '2021-12-27 12:33:02', '', 76, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-5.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 1, '2021-12-27 12:34:42', '2021-12-27 12:34:42', '', 'g12', '', 'publish', 'closed', 'closed', '', 'g12', '', '', '2022-01-03 12:04:46', '2022-01-03 12:04:46', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=78', 0, 'project', '', 0),
(79, 1, '2021-12-27 12:34:36', '2021-12-27 12:34:36', '', 'service-6', '', 'inherit', 'open', 'closed', '', 'service-6', '', '', '2021-12-27 17:13:51', '2021-12-27 17:13:51', '', 78, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-6.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2021-12-27 12:37:00', '2021-12-27 12:37:00', '', 'B12', '', 'publish', 'closed', 'closed', '', 'b12', '', '', '2022-01-03 12:05:34', '2022-01-03 12:05:34', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=80', 0, 'project', '', 0),
(81, 1, '2021-12-27 12:36:33', '2021-12-27 12:36:33', '', 'service-7', '', 'inherit', 'open', 'closed', '', 'service-7', '', '', '2021-12-27 12:36:33', '2021-12-27 12:36:33', '', 80, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-7.jpg', 0, 'attachment', 'image/jpeg', 0),
(82, 1, '2021-12-27 12:38:01', '2021-12-27 12:38:01', '', 'S54', '', 'publish', 'closed', 'closed', '', 's54', '', '', '2022-01-03 12:01:06', '2022-01-03 12:01:06', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=82', 0, 'project', '', 0),
(83, 1, '2021-12-27 12:37:31', '2021-12-27 12:37:31', '', 'service-8', '', 'inherit', 'open', 'closed', '', 'service-8', '', '', '2021-12-27 12:37:31', '2021-12-27 12:37:31', '', 82, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-8.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2021-12-27 12:38:49', '2021-12-27 12:38:49', '', 'k26', '', 'publish', 'closed', 'closed', '', 'k26', '', '', '2022-01-03 12:06:45', '2022-01-03 12:06:45', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=84', 0, 'project', '', 0),
(85, 1, '2021-12-27 12:38:19', '2021-12-27 12:38:19', '', 'service-9', '', 'inherit', 'open', 'closed', '', 'service-9', '', '', '2021-12-27 12:38:19', '2021-12-27 12:38:19', '', 84, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-9.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2021-12-27 12:39:21', '2021-12-27 12:39:21', '', 'A11', '', 'publish', 'closed', 'closed', '', 'a11', '', '', '2022-01-03 12:07:47', '2022-01-03 12:07:47', '', 0, 'http://pro-arkitektur-wp/build/?post_type=project&#038;p=86', 0, 'project', '', 0),
(87, 1, '2021-12-27 12:39:14', '2021-12-27 12:39:14', '', 'service-10', '', 'inherit', 'open', 'closed', '', 'service-10', '', '', '2021-12-27 12:39:14', '2021-12-27 12:39:14', '', 86, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/service-10.jpg', 0, 'attachment', 'image/jpeg', 0),
(88, 1, '2021-12-27 12:42:31', '2021-12-27 12:42:31', '', 'Home page', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2021-12-27 12:42:31', '2021-12-27 12:42:31', '', 5, 'http://pro-arkitektur-wp/build/?p=88', 0, 'revision', '', 0),
(89, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', 'Project information page', 'All', '', 'publish', 'closed', 'closed', '', 'all', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=89', 1, 'nav_menu_item', '', 0),
(90, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', ' ', '', '', 'publish', 'closed', 'closed', '', '90', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=90', 2, 'nav_menu_item', '', 0),
(91, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', ' ', '', '', 'publish', 'closed', 'closed', '', '91', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=91', 3, 'nav_menu_item', '', 0),
(92, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', ' ', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=92', 4, 'nav_menu_item', '', 0),
(93, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', ' ', '', '', 'publish', 'closed', 'closed', '', '93', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=93', 5, 'nav_menu_item', '', 0),
(94, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', ' ', '', '', 'publish', 'closed', 'closed', '', '94', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=94', 6, 'nav_menu_item', '', 0),
(95, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', ' ', '', '', 'publish', 'closed', 'closed', '', '95', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=95', 7, 'nav_menu_item', '', 0),
(96, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', ' ', '', '', 'publish', 'closed', 'closed', '', '96', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=96', 8, 'nav_menu_item', '', 0),
(97, 1, '2021-12-27 17:40:45', '2021-12-27 17:40:45', ' ', '', '', 'publish', 'closed', 'closed', '', '97', '', '', '2021-12-27 17:40:45', '2021-12-27 17:40:45', '', 0, 'http://pro-arkitektur-wp/build/?p=97', 9, 'nav_menu_item', '', 0),
(129, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Contacts', 'contacts', 'publish', 'closed', 'closed', '', 'field_61cb65f7c8fb3', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=129', 5, 'acf-field', '', 0),
(130, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Address', 'address', 'publish', 'closed', 'closed', '', 'field_61cb660fc8fb4', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=130', 6, 'acf-field', '', 0),
(131, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_61cb66b2c8fba', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 130, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=131', 0, 'acf-field', '', 0),
(132, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:4;s:9:\"new_lines\";s:2:\"br\";}', 'Text', 'text', 'publish', 'closed', 'closed', '', 'field_61cb66bfc8fbb', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 130, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=132', 1, 'acf-field', '', 0),
(133, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Phone numbers', 'phone_numbers', 'publish', 'closed', 'closed', '', 'field_61cb6627c8fb5', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=133', 7, 'acf-field', '', 0),
(134, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_61cb666dc8fb7', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 133, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=134', 0, 'acf-field', '', 0),
(135, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:9:\"Add Phone\";}', 'Phones', 'phones', 'publish', 'closed', 'closed', '', 'field_61cb6672c8fb8', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 133, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=135', 1, 'acf-field', '', 0),
(136, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Phone', 'phone', 'publish', 'closed', 'closed', '', 'field_61cb6677c8fb9', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 135, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=136', 0, 'acf-field', '', 0),
(137, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_61cb66e4c8fbc', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=137', 8, 'acf-field', '', 0),
(138, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_61cb66f2c8fbd', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 137, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=138', 0, 'acf-field', '', 0),
(139, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_61cb66f7c8fbe', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 137, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=139', 1, 'acf-field', '', 0),
(140, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:7:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:6:\"layout\";s:5:\"block\";s:10:\"sub_fields\";a:0:{}}', 'Social networks', 'social_networks', 'publish', 'closed', 'closed', '', 'field_61cb670ac8fbf', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 28, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=140', 9, 'acf-field', '', 0),
(141, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Label', 'label', 'publish', 'closed', 'closed', '', 'field_61cb6717c8fc0', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 140, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=141', 0, 'acf-field', '', 0),
(142, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Facebook', 'facebook', 'publish', 'closed', 'closed', '', 'field_61cb671fc8fc1', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 140, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=142', 1, 'acf-field', '', 0),
(143, 1, '2021-12-28 19:36:33', '2021-12-28 19:36:33', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Instagram', 'instagram', 'publish', 'closed', 'closed', '', 'field_61cb672ec8fc2', '', '', '2021-12-28 19:36:33', '2021-12-28 19:36:33', '', 140, 'http://pro-arkitektur-wp/build/?post_type=acf-field&p=143', 2, 'acf-field', '', 0),
(145, 1, '2022-01-03 12:00:53', '2022-01-03 12:00:53', '', 'S54-(15)', '', 'inherit', 'open', 'closed', '', 's54-15', '', '', '2022-01-03 12:00:53', '2022-01-03 12:00:53', '', 82, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/S54-15.jpg', 0, 'attachment', 'image/jpeg', 0),
(146, 1, '2022-01-03 12:02:05', '2022-01-03 12:02:05', '', 'LS-(1)', '', 'inherit', 'open', 'closed', '', 'ls-1', '', '', '2022-01-03 12:02:05', '2022-01-03 12:02:05', '', 70, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/LS-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(147, 1, '2022-01-03 12:03:07', '2022-01-03 12:03:07', '', 'M2-(3)', '', 'inherit', 'open', 'closed', '', 'm2-3-2', '', '', '2022-01-03 12:03:07', '2022-01-03 12:03:07', '', 72, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/M2-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(148, 1, '2022-01-03 12:03:22', '2022-01-03 12:03:22', '', 'T16', '', 'inherit', 'closed', 'closed', '', '74-autosave-v1', '', '', '2022-01-03 12:03:22', '2022-01-03 12:03:22', '', 74, 'http://pro-arkitektur-wp/build/?p=148', 0, 'revision', '', 0),
(149, 1, '2022-01-03 12:03:32', '2022-01-03 12:03:32', '', 'T16-(8)', '', 'inherit', 'open', 'closed', '', 't16-8', '', '', '2022-01-03 12:03:32', '2022-01-03 12:03:32', '', 74, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/T16-8.jpg', 0, 'attachment', 'image/jpeg', 0),
(150, 1, '2022-01-03 12:04:07', '2022-01-03 12:04:07', '', 'G35-(1)', '', 'inherit', 'open', 'closed', '', 'g35-1', '', '', '2022-01-03 12:04:07', '2022-01-03 12:04:07', '', 76, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/G35-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(151, 1, '2022-01-03 12:04:37', '2022-01-03 12:04:37', '', 'G12-(11)', '', 'inherit', 'open', 'closed', '', 'g12-11', '', '', '2022-01-03 12:04:37', '2022-01-03 12:04:37', '', 78, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/G12-11.jpg', 0, 'attachment', 'image/jpeg', 0),
(152, 1, '2022-01-03 12:05:20', '2022-01-03 12:05:20', '', 'B12-(3)', '', 'inherit', 'open', 'closed', '', 'b12-3-2', '', '', '2022-01-03 12:05:20', '2022-01-03 12:05:20', '', 80, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/B12-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(153, 1, '2022-01-03 12:06:22', '2022-01-03 12:06:22', '', 'K26-(4)', '', 'inherit', 'open', 'closed', '', 'k26-4-2', '', '', '2022-01-03 12:06:22', '2022-01-03 12:06:22', '', 84, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/K26-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(154, 1, '2022-01-03 12:07:41', '2022-01-03 12:07:41', '', 'A11-(1)', '', 'inherit', 'open', 'closed', '', 'a11-1', '', '', '2022-01-03 12:07:41', '2022-01-03 12:07:41', '', 86, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/A11-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(155, 1, '2022-01-03 12:07:53', '2022-01-03 12:07:53', '', 'H8A', '', 'inherit', 'closed', 'closed', '', '37-autosave-v1', '', '', '2022-01-03 12:07:53', '2022-01-03 12:07:53', '', 37, 'http://pro-arkitektur-wp/build/?p=155', 0, 'revision', '', 0),
(156, 1, '2022-01-03 12:08:08', '2022-01-03 12:08:08', '', 'H8A-(20)', '', 'inherit', 'open', 'closed', '', 'h8a-20', '', '', '2022-01-03 12:08:08', '2022-01-03 12:08:08', '', 37, 'http://pro-arkitektur-wp/build/wp-content/uploads/2021/12/H8A-20.jpg', 0, 'attachment', 'image/jpeg', 0),
(157, 1, '2022-01-03 12:45:27', '0000-00-00 00:00:00', '', 'Services', '', 'draft', 'closed', 'closed', '', '', '', '', '2022-01-03 12:45:27', '0000-00-00 00:00:00', '', 0, 'http://pro-arkitektur-wp/build/?page_id=157', 0, 'page', '', 0),
(158, 1, '2022-01-03 12:45:44', '2022-01-03 12:45:44', '', 'Services', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2022-01-03 12:45:44', '2022-01-03 12:45:44', '', 9, 'http://pro-arkitektur-wp/build/?p=158', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблиці `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Eng', 'en', 0),
(3, 'Eng', 'pll_en', 0),
(4, 'Nor', 'nn', 0),
(5, 'Nor', 'pll_nn', 0),
(6, 'pll_61c44fac2def7', 'pll_61c44fac2def7', 0),
(7, 'pll_61c44faea77cf', 'pll_61c44faea77cf', 0),
(8, 'Main menu EN', 'main-menu-en', 0),
(9, 'featured', 'featured', 0),
(10, 'pll_61c9f9e0c963a', 'pll_61c9f9e0c963a', 0),
(11, 'housing', 'housing', 0),
(12, 'pll_61c9f9e820ff4', 'pll_61c9f9e820ff4', 0),
(13, 'offices', 'offices', 0),
(14, 'pll_61c9f9ee2c284', 'pll_61c9f9ee2c284', 0),
(15, 'heritage monuments', 'heritage-monuments', 0),
(16, 'pll_61c9f9f424747', 'pll_61c9f9f424747', 0),
(17, 'cultural', 'cultural', 0),
(18, 'pll_61c9f9f996828', 'pll_61c9f9f996828', 0),
(19, 'utility', 'utility', 0),
(20, 'pll_61c9fa0011730', 'pll_61c9fa0011730', 0),
(21, 'executive projects', 'executive-projects', 0),
(22, 'pll_61c9fa05773c4', 'pll_61c9fa05773c4', 0),
(23, 'other', 'other', 0),
(24, 'pll_61c9fa0bc0c71', 'pll_61c9fa0bc0c71', 0),
(25, 'Projects Menu EN', 'projects-menu-en', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(1, 2, 0),
(1, 3, 0),
(1, 6, 0),
(3, 2, 0),
(5, 2, 0),
(5, 7, 0),
(7, 2, 0),
(9, 2, 0),
(9, 3, 0),
(9, 10, 0),
(11, 3, 0),
(11, 12, 0),
(13, 3, 0),
(13, 14, 0),
(15, 3, 0),
(15, 4, 0),
(15, 7, 0),
(15, 16, 0),
(17, 3, 0),
(17, 18, 0),
(19, 3, 0),
(19, 20, 0),
(21, 3, 0),
(21, 22, 0),
(23, 3, 0),
(23, 24, 0),
(37, 2, 0),
(37, 9, 0),
(37, 17, 0),
(38, 8, 0),
(39, 8, 0),
(40, 8, 0),
(70, 2, 0),
(70, 13, 0),
(70, 19, 0),
(72, 2, 0),
(72, 13, 0),
(72, 19, 0),
(74, 2, 0),
(74, 11, 0),
(74, 17, 0),
(76, 2, 0),
(76, 9, 0),
(76, 17, 0),
(78, 2, 0),
(78, 9, 0),
(78, 17, 0),
(80, 2, 0),
(80, 9, 0),
(80, 17, 0),
(82, 2, 0),
(82, 13, 0),
(82, 19, 0),
(84, 2, 0),
(84, 13, 0),
(84, 15, 0),
(84, 19, 0),
(86, 2, 0),
(86, 9, 0),
(86, 17, 0),
(89, 25, 0),
(90, 25, 0),
(91, 25, 0),
(92, 25, 0),
(93, 25, 0),
(94, 25, 0),
(95, 25, 0),
(96, 25, 0),
(97, 25, 0),
(157, 2, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'language', 'a:3:{s:6:\"locale\";s:5:\"en_GB\";s:3:\"rtl\";i:0;s:9:\"flag_code\";s:2:\"gb\";}', 0, 14),
(3, 3, 'term_language', '', 0, 9),
(4, 4, 'language', 'a:3:{s:6:\"locale\";s:5:\"nn_NO\";s:3:\"rtl\";i:0;s:9:\"flag_code\";s:2:\"no\";}', 0, 1),
(5, 5, 'term_language', '', 0, 0),
(6, 6, 'term_translations', 'a:1:{s:2:\"en\";i:1;}', 0, 1),
(7, 7, 'post_translations', 'a:2:{s:2:\"en\";i:5;s:2:\"nn\";i:15;}', 0, 2),
(8, 8, 'nav_menu', '', 0, 3),
(9, 9, 'projects-categories', '', 0, 5),
(10, 10, 'term_translations', 'a:1:{s:2:\"en\";i:9;}', 0, 1),
(11, 11, 'projects-categories', '', 0, 1),
(12, 12, 'term_translations', 'a:1:{s:2:\"en\";i:11;}', 0, 1),
(13, 13, 'projects-categories', '', 0, 4),
(14, 14, 'term_translations', 'a:1:{s:2:\"en\";i:13;}', 0, 1),
(15, 15, 'projects-categories', '', 0, 1),
(16, 16, 'term_translations', 'a:1:{s:2:\"en\";i:15;}', 0, 1),
(17, 17, 'projects-categories', '', 0, 6),
(18, 18, 'term_translations', 'a:1:{s:2:\"en\";i:17;}', 0, 1),
(19, 19, 'projects-categories', '', 0, 4),
(20, 20, 'term_translations', 'a:1:{s:2:\"en\";i:19;}', 0, 1),
(21, 21, 'projects-categories', '', 0, 0),
(22, 22, 'term_translations', 'a:1:{s:2:\"en\";i:21;}', 0, 1),
(23, 23, 'projects-categories', '', 0, 0),
(24, 24, 'term_translations', 'a:1:{s:2:\"en\";i:23;}', 0, 1),
(25, 25, 'nav_menu', '', 0, 9);

-- --------------------------------------------------------

--
-- Структура таблиці `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '144'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce&posts_list_mode=list'),
(20, 1, 'wp_user-settings-time', '1640688731'),
(21, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:19:\"pll_lang_switch_box\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";}'),
(23, 1, 'nav_menu_recently_edited', '25'),
(24, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(26, 1, 'edit_project_per_page', '20'),
(27, 1, 'session_tokens', 'a:1:{s:64:\"5b3f7ae77418f93152bee2c81e1a6bb4e9a3a076d9e0022a89b6ac4216185776\";a:4:{s:10:\"expiration\";i:1642420697;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36\";s:5:\"login\";i:1641211097;}}');

-- --------------------------------------------------------

--
-- Структура таблиці `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BK6JVd7z5la7dibuQVIa38ruZesZhk/', 'admin', 'example@gmail.com', 'http://pro-arkitektur-wp/build', '2021-12-23 10:20:18', '', 0, 'admin');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Індекси таблиці `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Індекси таблиці `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Індекси таблиці `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Індекси таблиці `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Індекси таблиці `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Індекси таблиці `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Індекси таблиці `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Індекси таблиці `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=642;

--
-- AUTO_INCREMENT для таблиці `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1065;

--
-- AUTO_INCREMENT для таблиці `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT для таблиці `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблиці `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблиці `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблиці `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблиці `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
