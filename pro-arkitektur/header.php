<?php
/**
 * @package WordPress
 * @subpackage PRO-Arkitektur
 * @since 1.0
 * @version 1.0
 */
$class = is_front_page() || is_single() ? ' absolute' : '';
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header class="ark-header d-lg-none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8">
                    <?php 
                    $logo = get_field('logo_dark', 'option');
                    if( $logo ) { ?>
                    <a class="ark-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <div class="col-4">
                    <div class="ark-mobile__btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <header class="ark-header d-none d-lg-block<?php echo $class; ?>">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-71">
                    <?php 
                    $logo = is_front_page() || is_single() ? get_field('logo', 'option') : get_field('logo_dark', 'option');
                    if( $logo ) { ?>
                    <a class="ark-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo $logo; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } ?>
                </div>
                <div class="col-lg-29">
                    <?php 
                    $languages = pll_the_languages( array('raw' => 1) );
                    if( has_nav_menu('main') || $languages ) { ?>
                    <div class="ark-menu__block">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'ark-menu__nav float-start'
                        ) );

                        if( $languages ){
                            $current_lang = $lang_list = '';
                            foreach ( $languages as $lang ) {
                                if( !$lang['current_lang'] ){
                                    $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['name'].'</a></li>';
                                }
                            } ?>
                            <div class="ark-lang__switcher float-end">
                                <ul>
                                    <?php echo $lang_list; ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>
    <div class="ark-mobile__menu">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php 
                    $phone_numbers = get_field('phone_numbers', 'option');
                    $email = get_field('email', 'option');
                    $social_networks = get_field('social_networks', 'option'); 
                    if( has_nav_menu('main') || $languages ) { ?>
                    <div class="ark-mobile__block">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'ark-mobile__nav'
                        ) );

                        if( $languages ){
                            $current_lang = $lang_list = '';
                            foreach ( $languages as $lang ) {
                                if( !$lang['current_lang'] ){
                                    $lang_list .= '<li><a href="'.$lang['url'].'">'.$lang['name'].'</a></li>';
                                }
                            } ?>
                            <div class="ark-lang__switcher text-center">
                                <ul>
                                    <?php echo $lang_list; ?>
                                </ul>
                            </div>
                        <?php }
                        if( $social_networks['facebook'] || $social_networks['instagram'] ) { ?>
                            <div class="ark-mobile__contacts text-center">
                                <?php if( $social_networks['facebook'] ) { ?>
                                <a href="<?php echo $social_networks['facebook']; ?>" target="_blank" class="social">
                                    <h5><?php _e('facebook', 'arkitektur'); ?></h5>
                                </a>
                                <?php } 
                                if( $social_networks['instagram'] ) { ?>
                                <a href="<?php echo $social_networks['instagram']; ?>" target="_blank" class="social">
                                    <h5><?php _e('instagram', 'arkitektur'); ?></h5>
                                </a>
                                <?php } ?>
                            </div>
                        <?php }
                        if( $phone_numbers['phones'] ) { ?>
                            <div class="ark-mobile__contacts phones text-center">
                                <?php foreach ( $phone_numbers['phones'] as $phone ) { ?>
                                <a href="tel:<?php echo $phone['phone']; ?>">
                                    <h5><?php echo $phone['phone']; ?></h5>
                                </a>
                                <?php } ?>
                            </div>
                        <?php }
                        if( $email['email'] ) { ?>
                            <div class="ark-mobile__contacts text-center">
                                <a href="mailto:<?php echo $email['email']; ?>">
                                    <h5><?php echo $email['email']; ?></h5>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <main>