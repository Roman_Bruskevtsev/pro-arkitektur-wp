<?php
/**
 * @package WordPress
 * @subpackage PRO-Arkitektur
 * @since 1.0
 * @version 1.0
 */

get_header(); 

get_template_part( 'template-parts/page/page_title' ); ?>

<section class="ark-projects__section">
    <div class="container-fluid">
    <?php if( has_nav_menu('projects') ) { ?>
        <div class="row">
            <div class="col">
                <?php wp_nav_menu( array(
                    'theme_location'        => 'projects',
                    'container'             => 'nav',
                    'container_class'       => 'ark-projects__nav float-start'
                ) ); ?>
            </div>
        </div>
    <?php } ?>
    </div>
</section>
<?php get_footer();