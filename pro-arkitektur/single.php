<?php
/**
 * @package WordPress
 * @subpackage PRO-Arkitektur
 * @since 1.0
 * @version 1.0
 */
get_header(); 

while ( have_posts() ) : the_post();
    $thumbnail = get_the_post_thumbnail_url(get_the_ID(), 'full');
    $gallery = get_field('gallery'); ?>
    <div class="ark-gallery__slider__section single">
        <div class="container-fluid nopadding">
            <div class="row">
                <div class="col-lg-71">
                    <?php if( $thumbnail || $gallery ) { ?>
                    <div class="ark-gallery__slider__wrapper">
                        <div class="ark-gallery__slider">
                            <div class="swiper-wrapper">
                            <?php if( $thumbnail ) { ?>
                                <div class="swiper-slide">
                                    <div class="image" style="background-image: url('<?php echo $thumbnail; ?>');"></div>
                                </div>
                            <?php } 
                            if( $gallery ) { 
                                foreach ( $gallery as $image ) { ?>
                                    <div class="swiper-slide">
                                        <div class="image" style="background-image: url('<?php echo $image; ?>');"></div>
                                    </div>
                                <?php }
                            } ?>
                            </div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                <?php } ?>
                </div>
                <div class="col-lg-29">
                    <div class="content__wrapper">
                        <div class="content">
                            <div class="show__info d-block d-lg-none"><span><?php _e('Info', 'arkitektur'); ?></span></div>
                            <h2 class="project__title"><?php the_title(); ?></h2>
                            <div class="project__details">
                                <?php if( get_field('type') ) { ?>
                                <div class="project__row">
                                    <div class="label">
                                        <h6><?php _e('Type:', 'arkitektur'); ?></h6>
                                    </div>
                                    <div class="value">
                                        <p><?php the_field('type'); ?></p>
                                    </div>
                                </div>
                                <?php } 
                                if( get_field('location') ) { ?>
                                <div class="project__row">
                                    <div class="label">
                                        <h6><?php _e('Location:', 'arkitektur'); ?></h6>
                                    </div>
                                    <div class="value">
                                        <p><?php the_field('location'); ?></p>
                                    </div>
                                </div>
                                <?php }
                                if( get_field('year') ) { ?>
                                <div class="project__row">
                                    <div class="label">
                                        <h6><?php _e('Year:', 'arkitektur'); ?></h6>
                                    </div>
                                    <div class="value">
                                        <p><?php the_field('year'); ?></p>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <?php if( get_field('description') ) { ?>
                            <div class="project__description"><?php the_field('description'); ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile;

get_footer();