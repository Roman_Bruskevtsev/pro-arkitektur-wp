<?php
/**
 * @package WordPress
 * @subpackage PRO-Arkitektur
 * @since 1.0
 * @version 1.0
 */
$term = get_queried_object();

get_header(); 

get_template_part( 'template-parts/page/page_title' ); ?>

<section class="ark-projects__section">
    <div class="container-fluid">
    <?php if( has_nav_menu('projects') ) { ?>
        <div class="row">
            <div class="col">
                <?php wp_nav_menu( array(
                    'theme_location'        => 'projects',
                    'container'             => 'nav',
                    'container_class'       => 'ark-projects__nav float-start'
                ) ); ?>
            </div>
        </div>
    <?php } 
    $paged = get_query_var('paged') ? get_query_var('paged') : 1; 
    $args = array(
        'post_type'         => 'project',
        'post_status'       => 'publish',
        'orderby'           => 'date',
        'order'             => 'DESC',
        'paged'             => $paged,
        'tax_query'         => array(
            array(
                'taxonomy'  => 'projects-categories',
                'field'     => 'term_id',
                'terms'     => $term->term_id
            )
        )
    );
    $query = new WP_Query( $args ); 
    if ( $query->have_posts() ) { ?>
        <div class="row">
            <div class="col">
                <div class="ark-posts__grid">
                    <div class="row">
                        <?php while ( $query->have_posts() ) { $query->the_post(); ?>
                        <div class="col-md-6">
                            <?php get_template_part( 'template-parts/project/content', 'post' ); ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if( $query->max_num_pages > 1 ) { ?>
        <div class="row">
            <div class="col">
                <div class="ark-button__group text-center">
                    <button id="load__projects" class="btn btn__primary" data-max-pages="<?php echo $query->max_num_pages; ?>" data-category="<?php echo $term->term_id; ?>"><?php _e('Load more projects', 'arkitektur'); ?></button>
                </div>
            </div>
        </div>
        <?php }
    } else { ?>
        <div class="row">
            <div class="col">
                <div class="ark-posts__grid">
                    <h3><?php _e('There are no projects in this category.', 'arkitektur'); ?></h3>
                </div>
            </div>
        </div>
    <?php } wp_reset_postdata(); ?>
    </div>
</section>
<?php get_footer();