<?php
/**
 * @package WordPress
 * @subpackage PRO-Arkitektur
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post();

    get_template_part( 'template-parts/page/page_title' );

    if( have_rows('content') ):
        while ( have_rows('content') ) : the_row();
            if( get_row_layout() == 'projects_slider_section' ):
                get_template_part( 'template-parts/page/projects_slider_section' );
            elseif ( get_row_layout() == 'services_section' ) :
                get_template_part( 'template-parts/page/services_section' );
            elseif ( get_row_layout() == 'map_contacts_block_section' ) :
                get_template_part( 'template-parts/page/map_contacts_block_section' );
            endif;
        endwhile;
    endif;

endwhile;

get_footer();