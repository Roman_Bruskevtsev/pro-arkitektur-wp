<?php
/**
 * 
 */
class ThemeSettingsClass {
	const SCRIPTS_VERSION    = '1.0.20';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_footer',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'init', array( $this, 'custom_posts_type') );
		add_action( 'init', array( $this, 'custom_taxonomy') );
		add_action( 'wp_ajax_load_projects', array( $this, 'load_projects' ) );
        add_action( 'wp_ajax_nopriv_load_projects', array( $this, 'load_projects' ) );
		
		add_filter( 'upload_mimes', array( $this, 'enable_svg_types'), 99 );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'arkitektur', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'arkitektur-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'particles', 'https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js', array( 'jquery' ), '', true );
    	
    	wp_enqueue_script( 'arkitektur', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
        
    }

    public function theme_setup(){
    	load_theme_textdomain( 'arkitektur' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );
	    add_theme_support( 'post-formats', array( 'video', 'audio' ) );

	    add_image_size( 'service-thumbnails', 360, 160, true );
	    add_image_size( 'new-thumbnails', 360, 240, true );
	    add_image_size( 'product-thumbnails', 848, 436, true );

	    register_nav_menus( array(
	        'main'          => __( 'Main Menu', 'arkitektur' ),
	        'projects'      => __( 'Projects Menu', 'arkitektur' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'arkitektur'),
		        'menu_title'    => __('Theme Settings', 'arkitektur'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1', 'arkitektur' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'arkitektur' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>',
	        	asyncUpload = '<?php echo admin_url('async-upload.php'); ?>';
	    </script>
	<?php }

	public function custom_posts_type(){
		$services_labels = array(
			'name'					=> __('Projects', 'arkitektur'),
			'singular_name'			=> __('Project', 'arkitektur'),
			'add_new'				=> __('Add Project', 'arkitektur'),
			'add_new_item'			=> __('Add New Project', 'arkitektur'),
			'edit_item'				=> __('Edit Project', 'arkitektur'),
			'new_item'				=> __('New Project', 'arkitektur'),
			'view_item'				=> __('View Project', 'arkitektur')
		);

		$services_args = array(
			'label'               => __('Projects', 'arkitektur'),
			'description'         => __('Project information page', 'arkitektur'),
			'labels'              => $services_labels,
			'supports'            => array( 'title', 'thumbnail' ),
			'taxonomies'          => array( '' ),
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => true,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => array(
				'slug'			  => 'projects'
			),
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-admin-home'
		);

		register_post_type( 'project', $services_args );
	}

	public function custom_taxonomy(){
		$taxonomy_labels = array(
			'name'                        => __('Projects categories', 'arkitektur'),
			'singular_name'               => __('Projects category', 'arkitektur'),
			'menu_name'                   => __('Projects categories', 'arkitektur'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'projects-categories',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'projects-categories', 'project', $taxonomy_args );
	}

	public function load_projects(){
		$paged = $_POST['page'];
		$args = array(
            'post_type'         => 'project',
            'post_status'       => 'publish',
            'orderby'           => 'date',
            'paged'             => $paged
        );

		if( $_POST['category'] != '*' ){
			$args['tax_query'] = array(
                array(
                    'taxonomy'  => 'projects-categories',
                    'field'     => 'term_id',
                    'terms'     => $_POST['category']
                )
            );
		}

		$query = new WP_Query( $args ); 

        if ( $query->have_posts() ) { 
        	while ( $query->have_posts() ) { $query->the_post(); ?>
                <div class="col-md-6">
                    <?php get_template_part( 'template-parts/project/content', 'post' ); ?>
                </div>
            <?php }
        } wp_reset_postdata();
        wp_die();
	}

    public function __return_false() {
        return false;
    }
}

$ThemeSettingsClass = new ThemeSettingsClass();