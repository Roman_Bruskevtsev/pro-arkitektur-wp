<?php
/**
 * @package WordPress
 * @subpackage PRO-Arkitektur
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <?php if( (!is_front_page()) && (!is_singular('project')) ) { ?>
    <footer class="ark-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <?php if( is_post_type_archive('project') || is_tax() ) { ?>
                    <div class="text-center">
                        <div class="ark-footer__arrow">
                            <h6><?php _e('Back to Top', 'arkitektur'); ?></h6>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-4"></div>
            </div>
            <div class="row">
                <div class="col-12">
                <?php if( get_field('copyrights', 'option') ) { ?>
                    <div class="ark-footer__copyrights float-start">
                        <p><?php the_field('copyrights', 'option'); ?></p>
                    </div>
                <?php } ?>
                    <a class="ark-footer__link float-end" href="https://vataga.agency/" target="_blank">Vataga Agency</a>
                </div>
            </div>
        </div>
    </footer>
    <?php }
    wp_footer(); ?>
</body>
</html>