'use strict';

class GeneralClass{
    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
        this.windowLoad();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(){
            AOS.init();
            general.mobileMenuInit();
            general.googleMapInit();
            general.projectsSliderInit();
            general.loadProjects();
            general.footerArrowInit();
        });
    }
    windowLoad(){
        window.onload = function() {
            
        }
    }

    mobileMenuInit(){
        let btn = document.querySelector('.ark-mobile__btn');
        if( !btn ) return false;
        let menu = document.querySelector('.ark-mobile__menu');
        btn.addEventListener('click', () => {
            btn.classList.toggle('show');
            menu.classList.toggle('show');
            document.querySelector('html').classList.toggle('show__menu');
        });
    }

    googleMapInit(){
        let map = document.querySelector('.ark-map');
        if( map ){
            let latitude = map.dataset.lat,
                longitude = map.dataset.lng,
                zoom = parseInt(map.dataset.zoom),
                markerIcon = map.dataset.marker,
                style = [
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e9e9e9"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 17
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 18
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dedede"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#333333"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            },
                            {
                                "lightness": 19
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#fefefe"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    }
                ],
                myLatlng = new google.maps.LatLng(latitude, longitude),
                styledMap = new google.maps.StyledMapType(style, { name: "Styled Map" }),
                mapOptions = {
                    zoom: zoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                },
                googleMap = new google.maps.Map(document.getElementById('google__map'), mapOptions),
                mark_location = new google.maps.LatLng(latitude, longitude);

                googleMap.mapTypes.set('map_style', styledMap);
                googleMap.setMapTypeId('map_style');

                let marker = new google.maps.Marker({
                    position: mark_location,
                    map: googleMap,
                    icon: markerIcon
                });

                marker.setMap(googleMap);
        }
    }

    projectsSliderInit(){
        let projectsSlider = document.querySelector('.ark-projects__slider');

        if( projectsSlider ) {

            let slider = new Swiper(projectsSlider, {
                direction: 'vertical',
                spaceBetween: 0,
                speed: 800,
                loop: true,
                slidesPerView: 1,
                effect: 'fade',
                fadeEffect: {
                    crossFade: true
                },
                noSwiping: true,
                noSwipingClass: 'swiper-slide',
                navigation: {
                    nextEl: '.swiper-button-next.text',
                    prevEl: '.swiper-button-prev.text',
                },
                keyboard: {
                    enabled: true,
                    onlyInViewport: false,
                }
            } );
        }

        let gallerySliders = document.querySelectorAll('.ark-gallery__slider');

        if( gallerySliders ) {
            let projectSlider;

            gallerySliders.forEach( ( gallery ) => {
                projectSlider = new Swiper( gallery, {
                    direction: 'horizontal',
                    loop: true,
                    slidesPerView: 1,
                    speed: 800,
                    navigation: {
                        nextEl: gallery.querySelector('.swiper-button-next'),
                        prevEl: gallery.querySelector('.swiper-button-prev'),
                    },
                    keyboard: {
                        enabled: true,
                        onlyInViewport: false,
                    }
                });
            });
        }

        let infoBtns = document.querySelectorAll('.show__info'),
            contentWrapper;

        if( infoBtns ){
            infoBtns.forEach( (btn) => {
                btn.addEventListener('click', () => {
                    contentWrapper = btn.closest('.content__wrapper');
                    contentWrapper.classList.toggle('show');
                    document.querySelector('html').classList.toggle('hide__scroll');
                });
            });
        }
    }

    loadProjects(){
        let loadBtn = document.getElementById('load__projects'),
            loadPage;
        if( !loadBtn ) return false;
        let xhr             = new XMLHttpRequest(),
            postsWrapper    = document.querySelector('.ark-posts__grid'),
            catID, maxPage;

        loadPage = loadPage ? loadPage : 2,

        loadBtn.addEventListener('click', () => {
            catID = loadBtn.dataset.category;
            maxPage = parseInt(loadBtn.dataset.maxPages);

            loadBtn.classList.add('load');
            postsWrapper.classList.add('load');
            
            if( maxPage >= loadPage ){
                xhr.open('POST', ajaxurl, true)
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;');
                xhr.onload = function () {
                    if ( this.status >= 200 ){
                        let res = this.response;
                        loadPage = loadPage + 1;

                        postsWrapper.querySelector('.row').insertAdjacentHTML('beforeEnd', res);
                        
                        loadBtn.classList.remove('load');
                        postsWrapper.classList.remove('load');
                        if( maxPage < loadPage ) loadBtn.classList.add('disabled');
                    }
                }
                xhr.onerror = function() {
                    console.log('connection error');
                };
                xhr.send('action=load_projects&page=' + loadPage + '&category=' + catID );
            }
        });
    }

    footerArrowInit(){
        let btn = document.querySelector('.ark-footer__arrow');

        if( !btn ) return false;

        btn.addEventListener('click', () => {
            window.scrollTo({
                top: 0,
                behavior: "smooth"
            });
        });
    }
}

let general = new GeneralClass();