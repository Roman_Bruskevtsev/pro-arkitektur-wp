<?php
$map = get_sub_field('map');
$address = get_field('address', 'option');
$phone_numbers = get_field('phone_numbers', 'option');
$email = get_field('email', 'option');
$social_networks = get_field('social_networks', 'option'); ?>
<section class="ark-map__contacts__block__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-71">
				<div id="google__map" class="ark-map" data-lat="<?php echo $map['latitude']; ?>" data-lng="<?php echo $map['longitude']; ?>" data-zoom="<?php echo $map['zoom']; ?>" data-marker="<?php echo $map['marker']; ?>"></div>
			</div>
			<div class="col-lg-29">
				<div class="ark-map__contacts__block">
				<?php if( $address['label'] && $address['text'] ) { ?>
					<div class="ark-map__contacts__row">
						<h6><?php echo $address['label']; ?></h6>
						<h3><?php echo $address['text']; ?></h3>
					</div>
				<?php } 
				if( $phone_numbers['label'] && $phone_numbers['phones'] ) { ?>
					<div class="ark-map__contacts__row phones">
						<h6><?php echo $phone_numbers['label']; ?></h6>
						<?php foreach ( $phone_numbers['phones'] as $phone ) { ?>
						<a href="tel:<?php echo $phone['phone']; ?>">
							<h3><?php echo $phone['phone']; ?></h3>
						</a>
						<?php } ?>
					</div>
				<?php }
				if( $email['label'] && $email['email'] ) { ?>
					<div class="ark-map__contacts__row">
						<h6><?php echo $email['label']; ?></h6>
						<a href="mailto:<?php echo $email['email']; ?>">
							<h3><?php echo $email['email']; ?></h3>
						</a>
					</div>
				<?php } 
				if( $social_networks['label'] && ( $social_networks['facebook'] || $social_networks['instagram'] ) ) { ?>
					<div class="ark-map__contacts__row">
						<h6><?php echo $social_networks['label']; ?></h6>
						<?php if( $social_networks['facebook'] ) { ?>
						<a href="<?php echo $social_networks['facebook']; ?>" target="_blank" class="social">
							<h5><?php _e('facebook', 'arkitektur'); ?></h5>
						</a>
						<?php } 
						if( $social_networks['instagram'] ) { ?>
						<a href="<?php echo $social_networks['instagram']; ?>" target="_blank" class="social">
							<h5><?php _e('instagram', 'arkitektur'); ?></h5>
						</a>
						<?php } ?>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>