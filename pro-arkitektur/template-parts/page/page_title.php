<?php 
$title = '';
if( is_page() ){
	$title = get_the_title();
} elseif ( is_post_type_archive('project') ) {
	$title = __('Projects', 'arkitektur');
} elseif ( is_tax() ) {
	// $title = single_term_title('', 0);
	$title = __('Projects', 'arkitektur');
}?>
<section class="ark-page__title__section">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<h1><?php echo $title; ?></h1>
			</div>
		</div>
	</div>
</section>