<?php 
$blocks = get_sub_field('service_blocks');
if( $blocks ) { ?>
<section class="ark-services__section">
	<div class="container-fluid padding-right">
		<div class="row">
		<?php foreach ( $blocks as $block ) { ?>
			<div class="col-lg-6">
				<div class="ark-service__block">
					<div class="content">
						<?php if( $block['title'] ) { ?>
							<h2><?php echo $block['title']; ?></h2>
						<?php }
						echo $block['text']; ?>
					</div>
				</div>
			</div>
		<?php } ?>
		</div>
	</div>
</section>
<?php } ?>