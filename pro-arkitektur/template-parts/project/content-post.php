<?php 
$thumbnail = get_the_post_thumbnail_url(get_the_ID(), 'full') ? ' style="background-image: url('.get_the_post_thumbnail_url(get_the_ID(), 'full').')"' : '';
?>
<a class="ark-project__block" href="<?php the_permalink(); ?>">
	<div class="thumbnail">
		<div class="image"<?php echo $thumbnail; ?>></div>
	</div>
	<div class="content">
		<h3><?php the_title(); ?></h3>
		<span class="text__link arrow"><?php _e('View project', 'arkitektur'); ?></span>
	</div>
</a>