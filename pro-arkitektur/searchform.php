<?php
/**
 * @package WordPress
 * @subpackage PRO-Arkitektur
 * @since 1.0
 * @version 1.0
 */
?>
<form role="search" method="get" class="acc-search__form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" class="search__field" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search__submit"></button>
</form>
